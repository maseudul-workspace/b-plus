package in.net.webinfotech.udaandemo.domain.interactors.impl;

import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.UpdateCartInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.udaandemo.domain.model.CommonResponse;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;

public class UpdateCartInteractorImpl extends AbstractInteractor implements UpdateCartInteractor {

    AppRepositoryImpl appRepository;
    Callback mCallback;
    String apiToken;
    int quantity;
    int cartId;

    public UpdateCartInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl appRepository, Callback mCallback, String apiToken, int quantity, int cartId) {
        super(threadExecutor, mainThread);
        this.appRepository = appRepository;
        this.mCallback = mCallback;
        this.quantity = quantity;
        this.cartId = cartId;
        this.apiToken = apiToken;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUpdateCartFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUpdateCartSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = appRepository.updateCart(apiToken, quantity, cartId);
        if (commonResponse == null) {
            notifyError("Something went wrong", commonResponse.loginError);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.loginError);
        } else {
           postMessage();
        }
    }
}
