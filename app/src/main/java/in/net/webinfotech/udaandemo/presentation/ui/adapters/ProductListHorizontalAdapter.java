package in.net.webinfotech.udaandemo.presentation.ui.adapters;

import android.content.Context;
import android.graphics.Paint;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.model.Product;
import in.net.webinfotech.udaandemo.util.GlideHelper;

/**
 * Created by Raj on 26-04-2019.
 */

public class ProductListHorizontalAdapter extends RecyclerView.Adapter<ProductListHorizontalAdapter.ViewHolder>{

    public interface Callback {
        void onProductClicked(int productId);
    }

    Context mContext;
    Product[] products;
    Callback mCallback;

    public ProductListHorizontalAdapter(Context mContext, Product[] products, Callback callback) {
        this.mContext = mContext;
        this.products = products;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_products_horizontal, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.txtViewProductName.setText(products[position].name);
        holder.txtViewSellingPrice.setText("Rs. " + products[position].price);
        if (products[position].mrp != null) {
            holder.txtViewPrice.setText("Rs. " + products[position].mrp);
        }
        GlideHelper.setImageView(mContext, holder.imgViewProduct, mContext.getResources().getString(R.string.api_url) + "product/image/thumb/" + products[position].mainImage);
        holder.txtViewPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        holder.imgViewProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onProductClicked(products[position].id);
            }
        });

    }

    @Override
    public int getItemCount() {
        return products.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_product)
        ImageView imgViewProduct;
        @BindView(R.id.txt_view_product_name)
        TextView txtViewProductName;
        @BindView(R.id.txt_view_selling_price)
        TextView txtViewSellingPrice;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
