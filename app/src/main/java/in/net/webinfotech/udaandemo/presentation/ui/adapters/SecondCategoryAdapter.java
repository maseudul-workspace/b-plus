package in.net.webinfotech.udaandemo.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.model.SecondCategory;
import in.net.webinfotech.udaandemo.util.GlideHelper;

/**
 * Created by Raj on 29-08-2019.
 */

public class SecondCategoryAdapter extends RecyclerView.Adapter<SecondCategoryAdapter.ViewHolder> {

    public interface Callback {
        void onCategoryClicked(int id);
    }

    Context mContext;
    SecondCategory[] secondCategories;
    Callback mCallback;

    public SecondCategoryAdapter(Context mContext, SecondCategory[] secondCategories, Callback callback) {
        this.mContext = mContext;
        this.secondCategories = secondCategories;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_second_category, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewSecondCategory, mContext.getResources().getString(R.string.api_url) + "second/category/image/thumb/" + secondCategories[position].image, 100);
        holder.txtViewSecondCategory.setText(secondCategories[position].name);
        holder.txtViewTotalProducts.setText(secondCategories[position].totalProducts + " products");
        holder.imgViewSecondCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onCategoryClicked(secondCategories[position].id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return secondCategories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_second_category)
        ImageView imgViewSecondCategory;
        @BindView(R.id.txt_view_second_category_name)
        TextView txtViewSecondCategory;
        @BindView(R.id.txt_view_total_products)
        TextView txtViewTotalProducts;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
