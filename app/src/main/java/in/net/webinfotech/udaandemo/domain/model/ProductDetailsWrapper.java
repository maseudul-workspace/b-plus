package in.net.webinfotech.udaandemo.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 03-09-2019.
 */

public class ProductDetailsWrapper {

    @SerializedName("status")
    @Expose
    public Boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("current_page")
    @Expose
    public int currentPage;

    @SerializedName("total_page")
    @Expose
    public int totalPage;

    @SerializedName("data")
    @Expose
    public ProductDetails productDetails;

}
