package in.net.webinfotech.udaandemo.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.GetProductsBySellerInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.impl.GetProductsBySellerInteractorImpl;
import in.net.webinfotech.udaandemo.domain.model.Product;
import in.net.webinfotech.udaandemo.domain.model.SellersList;
import in.net.webinfotech.udaandemo.presentation.presenters.ProductsBySellerPresenter;
import in.net.webinfotech.udaandemo.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.ProductListVerticalAdapter;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.SellerProductListAdapter;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;

/**
 * Created by Raj on 29-08-2019.
 */

public class ProductsBySellerPresenterImpl extends AbstractPresenter implements ProductsBySellerPresenter, GetProductsBySellerInteractor.Callback, SellerProductListAdapter.Callback {

    Context mContext;
    ProductsBySellerPresenter.View mView;
    GetProductsBySellerInteractorImpl getProductsBySellerInteractor;
    SellerProductListAdapter adapter;
    SellersList[] newSellersLists;

    public ProductsBySellerPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchProductsBySeller(int id, int pageNo, String type) {
        if (type.equals("refresh")) {
            newSellersLists = null;
        }
        getProductsBySellerInteractor = new GetProductsBySellerInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), id, pageNo);
        getProductsBySellerInteractor.execute();
    }

    @Override
    public void onGettingProductsListSuccess(SellersList[] sellersLists, int totalPage) {

        if(sellersLists.length > 0){
            SellersList[] tempSellerLists;
            tempSellerLists = newSellersLists;
            try {
                int len1 = tempSellerLists.length;
                int len2 = sellersLists.length;
                newSellersLists = new SellersList[len1 + len2];
                System.arraycopy(tempSellerLists, 0, newSellersLists, 0, len1);
                System.arraycopy(sellersLists, 0, newSellersLists, len1, len2);
                adapter.updateDataSet(newSellersLists);
                adapter.notifyDataSetChanged();
                mView.hidePaginationProgressBar();
            }catch (NullPointerException e){
                newSellersLists = sellersLists;
                adapter = new SellerProductListAdapter(mContext, sellersLists, this);
                mView.loadAdapter(adapter, totalPage);
                mView.hideLoader();
            }
        }
    }

    @Override
    public void onGettingProductsListFail(String errorMsg) {
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        mView.hideLoader();
        mView.hidePaginationProgressBar();
    }

    @Override
    public void onSeeAllClicked(int subcategoryId, int sellerId) {
        mView.goToProductList(subcategoryId, sellerId);
    }

    @Override
    public void onProductClicked(int productId) {
        mView.goToProductDetails(productId);
    }
}
