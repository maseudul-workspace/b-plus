package in.net.webinfotech.udaandemo.presentation.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;

import java.util.ArrayList;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.udaandemo.domain.model.FilterData;
import in.net.webinfotech.udaandemo.presentation.presenters.ProductListPresenter;
import in.net.webinfotech.udaandemo.presentation.presenters.impl.ProductListPresenterImpl;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.ProductListVerticalAdapter;
import in.net.webinfotech.udaandemo.presentation.ui.dialogs.FilterByDialog;
import in.net.webinfotech.udaandemo.presentation.ui.dialogs.SortByBottomSheetDialog;
import in.net.webinfotech.udaandemo.threading.MainThreadImpl;

public class ProductListActivity extends AppCompatActivity implements ProductListPresenter.View, SortByBottomSheetDialog.Callback, FilterByDialog.Callback {

    ProductListPresenterImpl mPresenter;
    SortByBottomSheetDialog sortByBottomSheetDialog;
    String selectedText = "Newest Arrivals";
    FilterByDialog filterByDialog;
    int subcategoryId;
    int sellerId;
    int pageNo = 1;
    String sort = "newest";
    @BindView(R.id.recycler_view_product_list_vertical)
    RecyclerView recyclerView;
    ArrayList<Integer> sellerIds;
    ArrayList<Integer> brandIds;
    ArrayList<Integer> colorIds;
    int minPrice = 0;
    int maxPrice = 0;
    boolean isFirstLoad = true;
    ProgressDialog progressDialog;
    @BindView(R.id.pagination_progress_bar)
    View paginationProgressbar;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int totalPage = 1;
    LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        ButterKnife.bind(this);
        initialiseArraylist();
        setUpProgressDialog();
        initialiseFilterByDialog();
        filterByDialog.setUpDialogView();
        initialisePresenter();
        subcategoryId = getIntent().getIntExtra("subcategoryId", 0);
        sellerId = getIntent().getIntExtra("sellerId", 0);
        mPresenter.fetchProductListWithFilters(subcategoryId, sellerId, pageNo, "refresh");
        showLoader();
    }

    public void initialiseArraylist() {
        sellerIds = new ArrayList<>();
        brandIds = new ArrayList<>();
        colorIds = new ArrayList<>();
    }

    public void initialisePresenter() {
        mPresenter = new ProductListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void initialiseFilterByDialog(){
        filterByDialog = new FilterByDialog(this, this, this::onFiltersApplied);
    }
    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.layout_sort_by) void onSortByClicked() {
        Bundle args = new Bundle();
        args.putString("selectedText", selectedText);
        sortByBottomSheetDialog = new SortByBottomSheetDialog();
        sortByBottomSheetDialog.setArguments(args);
        sortByBottomSheetDialog.show(getSupportFragmentManager(), "Sort By");
    }

    @OnClick(R.id.layout_filter_by) void onLayoutFilterByClicked() {
        filterByDialog.showDialog();
    }

    @Override
    public void onRadioBtnSelected(String msg) {
        this.selectedText = msg;
        isScrolling = false;
        pageNo = 1;
        totalPage = 1;
        isFirstLoad = false;
        switch (msg) {
            case "Newest Arrivals":
                sort = "newest";
                break;
            case "Title: A - Z":
                sort = "title_asc";
                break;
            case "Title: Z - A":
                sort = "title_dsc";
                break;
            case "Price: High to Low":
                sort = "high";
                break;
            case "Price: Low to High":
                sort = "low";
                break;
        }
        mPresenter.fetchProductListWithoutFilters(subcategoryId, sellerIds, brandIds, colorIds, minPrice, maxPrice, sort, pageNo, "refresh");
        showLoader();
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void loadFilters(FilterData filterData) {
        filterByDialog.setData(filterData);
    }

    @Override
    public void loadAdapter(ProductListVerticalAdapter adapter, int totalPage) {
        this.totalPage = totalPage;
        recyclerView.setVisibility(View.VISIBLE);
        layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        showPaginationProgressBar();
                        if(isFirstLoad){
                            mPresenter.fetchProductListWithFilters(subcategoryId, sellerId, pageNo, "");
                        }else{
                            mPresenter.fetchProductListWithoutFilters(subcategoryId, sellerIds, brandIds, colorIds, minPrice, maxPrice, sort, pageNo, "");
                        }
                    }
                }
            }
        });
    }

    @Override
    public void goToProductDetails(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.hide();
    }

    @Override
    public void hidePaginationProgressBar() {
        paginationProgressbar.setVisibility(View.GONE);
    }

    public void showPaginationProgressBar() {
        paginationProgressbar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFiltersApplied(ArrayList<Integer> sellerIds, ArrayList<Integer> brandIds, ArrayList<Integer> colorIds, int minPrice, int maxPrice) {
        isFirstLoad = false;
        isScrolling = false;
        pageNo = 1;
        totalPage = 1;
        this.sellerIds = sellerIds;
        this.brandIds = brandIds;
        this.colorIds = colorIds;
        this.maxPrice = maxPrice;
        this.minPrice = minPrice;
        mPresenter.fetchProductListWithoutFilters(subcategoryId, sellerIds, brandIds, colorIds, minPrice, maxPrice, sort, pageNo, "refresh");
        showLoader();
        recyclerView.setVisibility(View.GONE);
    }
}
