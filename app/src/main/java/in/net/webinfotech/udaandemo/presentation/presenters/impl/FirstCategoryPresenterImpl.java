package in.net.webinfotech.udaandemo.presentation.presenters.impl;

import android.content.Context;
import android.widget.ImageView;

import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.GetFirstCategoriesInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.impl.GetFirstCategoriesInteractorImpl;
import in.net.webinfotech.udaandemo.domain.model.FirstCategory;
import in.net.webinfotech.udaandemo.presentation.presenters.FirstCategoryPresenter;
import in.net.webinfotech.udaandemo.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.FirstCategoryAdapter;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;

/**
 * Created by Raj on 28-08-2019.
 */

public class FirstCategoryPresenterImpl extends AbstractPresenter implements FirstCategoryPresenter, GetFirstCategoriesInteractor.Callback, FirstCategoryAdapter.Callback {

    Context mContext;
    FirstCategoryPresenter.View mView;
    GetFirstCategoriesInteractorImpl getFirstCategoriesInteractor;
    FirstCategoryAdapter adapter;

    public FirstCategoryPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchFirstCategory(int id) {
        getFirstCategoriesInteractor = new GetFirstCategoriesInteractorImpl(mExecutor, mMainThread,this, new AppRepositoryImpl(), id);
        getFirstCategoriesInteractor.execute();
    }

    @Override
    public void onGettingFirstCategoriesSuccess(FirstCategory[] firstCategories) {
        adapter = new FirstCategoryAdapter(mContext, firstCategories, this);
        mView.loadData(adapter);
    }

    @Override
    public void onGettingFirstCategoriesFail(String erroMsg) {

    }

    @Override
    public void onFirstCategoryClicked(int id, String name, String image, ImageView imageView) {
        mView.goToSecondCategory(id, name, image, imageView);
    }
}
