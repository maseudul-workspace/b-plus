package in.net.webinfotech.udaandemo.domain.interactors;

import in.net.webinfotech.udaandemo.domain.model.ProductDetails;

/**
 * Created by Raj on 03-09-2019.
 */

public interface GetProductDetailsInteractor {
    interface Callback {
        void onGettingProductDetailSuccess(ProductDetails productDetails);
        void onGettingProductDetailsFail(String errorMsg);
    }
}
