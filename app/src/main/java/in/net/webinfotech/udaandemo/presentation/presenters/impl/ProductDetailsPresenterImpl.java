package in.net.webinfotech.udaandemo.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import in.net.webinfotech.udaandemo.AndroidApplication;
import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.AddToCartInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.FetchCartsInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.GetProductDetailsInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.impl.AddToCartInteractorImpl;
import in.net.webinfotech.udaandemo.domain.interactors.impl.FetchCartInteractorImpl;
import in.net.webinfotech.udaandemo.domain.interactors.impl.GetProductDetailsInteractorImpl;
import in.net.webinfotech.udaandemo.domain.model.Cart;
import in.net.webinfotech.udaandemo.domain.model.ProductDetails;
import in.net.webinfotech.udaandemo.domain.model.User;
import in.net.webinfotech.udaandemo.presentation.presenters.ProductDetailsPresenter;
import in.net.webinfotech.udaandemo.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.ProductListHorizontalAdapter;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;

/**
 * Created by Raj on 03-09-2019.
 */

public class ProductDetailsPresenterImpl extends AbstractPresenter implements   ProductDetailsPresenter,
                                                                                GetProductDetailsInteractor.Callback,
                                                                                AddToCartInteractor.Callback,
                                                                                FetchCartsInteractor.Callback,
                                                                                ProductListHorizontalAdapter.Callback
{

    Context mContext;
    ProductDetailsPresenter.View mView;
    GetProductDetailsInteractorImpl getProductDetailsInteractor;
    AndroidApplication androidApplication;
    int colorId;
    AddToCartInteractorImpl addToCartInteractor;
    FetchCartInteractorImpl fetchCartInteractor;
    ProductListHorizontalAdapter productListHorizontalAdapter;

    public ProductDetailsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void getProductDetails(int productId) {
        getProductDetailsInteractor = new GetProductDetailsInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), productId);
        getProductDetailsInteractor.execute();
    }

    @Override
    public void addToCart(int productId, int colorId, int quantity) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        addToCartInteractor = new AddToCartInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, productId, quantity, colorId);
        addToCartInteractor.execute();
    }

    @Override
    public void fetchCartCount() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        if (user != null) {
            fetchCartInteractor = new FetchCartInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId);
            fetchCartInteractor.execute();
        }
    }

    @Override
    public void onGettingProductDetailSuccess(ProductDetails productDetails) {
        mView.loadData(productDetails);
        productListHorizontalAdapter = new ProductListHorizontalAdapter(mContext, productDetails.relatedProducts, this);
        mView.loadRelatedProducts(productListHorizontalAdapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingProductDetailsFail(String errorMsg) {
        mView.hideLoader();
    }

    @Override
    public void onAddToCartSuccess() {
        Toast.makeText(mContext, "Added To Cart", Toast.LENGTH_SHORT).show();
        fetchCartCount();
        mView.hideProgressDialog();
    }

    @Override
    public void onAddToCartFail(String errorMsg, int loginError) {
        mView.hideProgressDialog();
        if (loginError == 1) {
            mView.goToLoginActivity();
        } else {
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onGettingCartDetailsSuccess(Cart[] carts) {
        mView.loadCartCount(carts.length);
    }

    @Override
    public void onGettingCartDetailsFail(String errorMsg, int loginError) {
        mView.loadCartCount(0);
    }

    @Override
    public void onProductClicked(int productId) {
        mView.onProductClicked(productId);
    }
}
