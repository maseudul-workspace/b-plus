package in.net.webinfotech.udaandemo.domain.interactors;

import in.net.webinfotech.udaandemo.domain.model.GatewayOrder;

public interface InitiatePaymentInteractor {
    interface Callback {
        void onInitiatePaymentSuccess(GatewayOrder gatewayOrder);
        void onInitiatePaymentFail(String errorMsg);
    }
}
