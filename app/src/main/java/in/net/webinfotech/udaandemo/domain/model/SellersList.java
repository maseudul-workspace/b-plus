package in.net.webinfotech.udaandemo.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 29-08-2019.
 */

public class SellersList {

    @SerializedName("seller_id")
    @Expose
    public int sellerId;

    @SerializedName("seller_name")
    @Expose
    public String sellerName;

    @SerializedName("seller_state")
    @Expose
    public String sellerState;

    @SerializedName("seller_city")
    @Expose
    public String sellerCity;

    @SerializedName("second_category")
    @Expose
    public int secondCategoryId;

    @SerializedName("product")
    @Expose
    public Product[] products;

}
