package in.net.webinfotech.udaandemo.domain.interactors;

public interface SignUpInteractor {
    interface Callback {
        void onSignUpSuccess();
        void onSignUpFailed(String errorMsg);
    }
}
