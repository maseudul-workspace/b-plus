package in.net.webinfotech.udaandemo.domain.interactors.impl;

import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.DeleteShippingAddressInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.udaandemo.domain.model.CommonResponse;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;

public class DeleteShippingAddressInteractorImpl extends AbstractInteractor implements DeleteShippingAddressInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int addressId;

    public DeleteShippingAddressInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int addressId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.addressId = addressId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddressDeleteFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddressDeleteSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.deleteShippingAddress(apiToken, addressId);
        if (commonResponse == null) {
            notifyError("Something went wrong", commonResponse.loginError);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.loginError);
        } else {
            postMessage();
        }
    }
}
