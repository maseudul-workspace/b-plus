package in.net.webinfotech.udaandemo.presentation.presenters;

import in.net.webinfotech.udaandemo.domain.model.ProductDetails;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.ProductListHorizontalAdapter;

/**
 * Created by Raj on 03-09-2019.
 */

public interface ProductDetailsPresenter {
    void getProductDetails(int productId);
    void addToCart(int productId, int colorId, int quantity);
    void fetchCartCount();
    interface View {
        void loadData(ProductDetails productDetails);
        void showLoader();
        void hideLoader();
        void showProgressDialog();
        void hideProgressDialog();
        void loadCartCount(int count);
        void loadRelatedProducts(ProductListHorizontalAdapter adapter);
        void onProductClicked(int productId);
        void goToLoginActivity();
    }
}
