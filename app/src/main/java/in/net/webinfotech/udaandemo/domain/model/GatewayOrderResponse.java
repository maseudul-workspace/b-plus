package in.net.webinfotech.udaandemo.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GatewayOrderResponse {

    @SerializedName("success")
    @Expose
    public boolean success = true;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("order")
    @Expose
    public GatewayOrder gatewayOrder;
}
