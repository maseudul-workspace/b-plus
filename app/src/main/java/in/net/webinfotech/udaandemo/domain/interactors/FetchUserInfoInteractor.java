package in.net.webinfotech.udaandemo.domain.interactors;

import in.net.webinfotech.udaandemo.domain.model.User;
import in.net.webinfotech.udaandemo.domain.model.UserDetails;
import in.net.webinfotech.udaandemo.domain.model.UserWrapper;

public interface FetchUserInfoInteractor {
    interface Callback {
        void onGettingUserInfoSuccess(UserDetails userDetails);
        void onGettingUserInfoFail(String errorMsg, int loginError);
    }
}
