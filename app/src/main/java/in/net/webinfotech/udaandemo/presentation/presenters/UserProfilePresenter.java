package in.net.webinfotech.udaandemo.presentation.presenters;

import in.net.webinfotech.udaandemo.domain.model.User;
import in.net.webinfotech.udaandemo.domain.model.UserDetails;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.CityListDialogAdapter;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.StateListDialogAdapter;

public interface UserProfilePresenter {
    void fetchStateList();
    void fetchCityList();
    void fetchUserProfile();
    void updateUserProfile(String gender, String name, String mobile, String pin, String address);
    interface View {
        void loadData(UserDetails userDetails);
        void showLoader();
        void hideLoader();
        void hideStateRecyclerView();
        void hideCityRecyclerView();
        void loadStateAdapter(StateListDialogAdapter stateListDialogAdapter);
        void loadCitiesAdapter(CityListDialogAdapter cityListDialogAdapter);
        void setStateName(String stateName);
        void setCityName(String cityName);
        void goToLoginActivity();
    }
}
