package in.net.webinfotech.udaandemo.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.model.FirstCategory;
import in.net.webinfotech.udaandemo.util.GlideHelper;

/**
 * Created by Raj on 28-08-2019.
 */

public class FirstCategoryAdapter extends RecyclerView.Adapter<FirstCategoryAdapter.ViewHolder>{

    public interface Callback {
        void onFirstCategoryClicked(int id, String name, String image, ImageView imageView);
    }

    Context mContext;
    FirstCategory[] firstCategories;
    Callback mCallback;

    public FirstCategoryAdapter(Context mContext, FirstCategory[] firstCategories, Callback callback) {
        this.mContext = mContext;
        this.firstCategories = firstCategories;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_first_category, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        GlideHelper.setImageView(mContext, holder.imgViewFirstCategory, mContext.getResources().getString(R.string.api_url) + "first/category/image/" + firstCategories[position].image);
        holder.txtViewFirstCategory.setText(firstCategories[position].name);
        holder.imgViewFirstCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onFirstCategoryClicked(firstCategories[position].id, firstCategories[position].name, firstCategories[position].image, holder.imgViewFirstCategory);
            }
        });
    }

    @Override
    public int getItemCount() {
        return firstCategories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_first_category)
        ImageView imgViewFirstCategory;
        @BindView(R.id.txt_view_first_category_name)
        TextView txtViewFirstCategory;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
