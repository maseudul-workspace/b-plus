package in.net.webinfotech.udaandemo.domain.interactors.impl;

import java.util.ArrayList;

import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.GetProductListWithoutFilterInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.udaandemo.domain.model.Product;
import in.net.webinfotech.udaandemo.domain.model.ProductDetails;
import in.net.webinfotech.udaandemo.domain.model.ProductListWrapper;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;

public class GetProductListWithoutFilterInteractorImpl extends AbstractInteractor implements GetProductListWithoutFilterInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int secondCategory;
    ArrayList<Integer> sellerIds;
    ArrayList<Integer> brandIds;
    ArrayList<Integer> colorIds;
    int minPrice;
    int maxPrice;
    String sortBy;
    int page;

    public GetProductListWithoutFilterInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int secondCategory, ArrayList<Integer> sellerIds, ArrayList<Integer> brandIds, ArrayList<Integer> colorIds, int minPrice, int maxPrice, String sortBy, int page) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.secondCategory = secondCategory;
        this.sellerIds = sellerIds;
        this.brandIds = brandIds;
        this.colorIds = colorIds;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.sortBy = sortBy;
        this.page = page;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductListWihoutFilterFail(errorMsg);
            }
        });
    }

    private void postMessage(final Product[] products, int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductListWithoutFilterSuccess(products, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final ProductListWrapper productListWrapper = mRepository.fetchProductList(secondCategory, sellerIds, brandIds, colorIds, minPrice, maxPrice, sortBy, page);
        if (productListWrapper == null) {
           notifyError("Something went wrong");
        } else if (!productListWrapper.status) {
            notifyError(productListWrapper.message);
        } else {
            postMessage(productListWrapper.products, productListWrapper.totalPage);
        }
    }
}
