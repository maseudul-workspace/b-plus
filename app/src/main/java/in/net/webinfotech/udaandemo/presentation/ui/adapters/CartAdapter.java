package in.net.webinfotech.udaandemo.presentation.ui.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.model.Cart;
import in.net.webinfotech.udaandemo.util.GlideHelper;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {

    public interface Callback {
        void onDeleteClicked(int cartId);
        void onUpdateClicked(int cartId, int qty);
    }

    Context mContext;
    Cart[] carts;
    Callback mCallback;

    public CartAdapter(Context mContext, Cart[] carts, Callback mCallback) {
        this.mContext = mContext;
        this.carts = carts;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_cart, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewProductTitle.setText(carts[position].productName);
        holder.txtViewQty.setText(Integer.toString(carts[position].quantity));
        GlideHelper.setImageView(mContext, holder.imgViewProduct, mContext.getResources().getString(R.string.api_url) + "product/image/thumb/" + carts[position].productImage);
        holder.imgViewCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Are You Sure ?");
                builder.setMessage("You are about to delete a remove an item from your cart. Do you really want to proceed ?");
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.onDeleteClicked(carts[position].cartId);
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                builder.show();
            }
        });
        holder.imgViewPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                carts[position].quantity = carts[position].quantity + 1;
                holder.txtViewQty.setText(Integer.toString(carts[position].quantity));
            }
        });
        holder.imgViewMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (carts[position].minOrderQty > carts[position].quantity) {
                    carts[position].quantity = carts[position].quantity - 1;
                    holder.txtViewQty.setText(Integer.toString(carts[position].quantity));
                }
            }
        });
        holder.txtViewPrice.setText("Rs." + Double.toString(carts[position].productPrice));
        holder.txtViewMrp.setText("Rs. " + Double.toString(carts[position].productMRP));
        holder.txtViewPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        holder.btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onUpdateClicked(carts[position].cartId, carts[position].quantity);
            }
        });
    }

    @Override
    public int getItemCount() {
        return carts.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_cross)
        ImageView imgViewCross;
        @BindView(R.id.img_view_product)
        ImageView imgViewProduct;
        @BindView(R.id.txt_view_product_title)
        TextView txtViewProductTitle;
        @BindView(R.id.txt_view_qty)
        TextView txtViewQty;
        @BindView(R.id.img_view_plus)
        ImageView imgViewPlus;
        @BindView(R.id.img_view_minus)
        ImageView imgViewMinus;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;
        @BindView(R.id.txt_view_mrp)
        TextView txtViewMrp;
        @BindView(R.id.btn_update)
        Button btnUpdate;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
