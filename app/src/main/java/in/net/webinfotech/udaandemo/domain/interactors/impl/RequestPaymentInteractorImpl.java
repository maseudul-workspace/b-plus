package in.net.webinfotech.udaandemo.domain.interactors.impl;

import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.RequestPaymentInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.udaandemo.domain.model.PaymentRequestResponse;
import in.net.webinfotech.udaandemo.repository.PaymentRepositoryImpl;

public class RequestPaymentInteractorImpl extends AbstractInteractor implements RequestPaymentInteractor {

    PaymentRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;
    String id;

    public RequestPaymentInteractorImpl(Executor threadExecutor, MainThread mainThread, PaymentRepositoryImpl mRepository, Callback mCallback, String authorization, String id) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.id = id;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRequestPaymentFail(errorMsg);
            }
        });
    }

    private void postMessage(PaymentRequestResponse paymentRequestResponse){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRequestPaymentSuccess(paymentRequestResponse);
            }
        });
    }

    @Override
    public void run() {
        final PaymentRequestResponse paymentRequestResponse = mRepository.requestPayment(authorization, id);
        if (paymentRequestResponse == null) {
            notifyError("Something went wrong");
        } else {
            postMessage(paymentRequestResponse);
        }
    }
}
