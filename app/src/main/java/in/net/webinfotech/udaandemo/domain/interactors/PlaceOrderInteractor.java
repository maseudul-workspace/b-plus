package in.net.webinfotech.udaandemo.domain.interactors;

import in.net.webinfotech.udaandemo.domain.model.OrderPlaceData;

public interface PlaceOrderInteractor {
    interface Callback {
        void onPlaceOrderSuccess(OrderPlaceData orderPlaceData);
        void onPlaceOrderFail(String errorMsg, int loginError);
    }
}
