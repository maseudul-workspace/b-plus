package in.net.webinfotech.udaandemo.presentation.presenters;

import in.net.webinfotech.udaandemo.presentation.ui.adapters.SecondCategoryAdapter;

/**
 * Created by Raj on 29-08-2019.
 */

public interface SecondCategoryPresenter {

    void fetchSecondCategories(int id);

    interface View {
        void loadAdapter(SecondCategoryAdapter adapter);
        void goToProductsBySellerList(int id);
    }
}
