package in.net.webinfotech.udaandemo.presentation.presenters.impl;

import android.content.Context;

import in.net.webinfotech.udaandemo.AndroidApplication;
import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.FetchCartsInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.GetMainDataInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.impl.FetchCartInteractorImpl;
import in.net.webinfotech.udaandemo.domain.interactors.impl.GetMainDataInteractorImpl;
import in.net.webinfotech.udaandemo.domain.model.Cart;
import in.net.webinfotech.udaandemo.domain.model.MainData;
import in.net.webinfotech.udaandemo.domain.model.User;
import in.net.webinfotech.udaandemo.presentation.presenters.MainPresenter;
import in.net.webinfotech.udaandemo.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;

/**
 * Created by Raj on 28-08-2019.
 */

public class MainPresenterImpl extends AbstractPresenter implements MainPresenter, GetMainDataInteractor.Callback, FetchCartsInteractor.Callback {

    Context mContext;
    MainPresenter.View mView;
    GetMainDataInteractorImpl getMainDataInteractor;
    AndroidApplication androidApplication;
    FetchCartInteractorImpl fetchCartInteractor;


    public MainPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchHomeData() {
        getMainDataInteractor = new GetMainDataInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl());
        getMainDataInteractor.execute();
    }

    @Override
    public void fetchCartItems() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        if (user != null) {
            fetchCartInteractor = new FetchCartInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId);
            fetchCartInteractor.execute();
        }
    }

    @Override
    public void onGettingMainDataSuccess(MainData mainData) {
        mView.loadMainData(mainData);
    }

    @Override
    public void onGettingMainDataFail() {

    }

    @Override
    public void onGettingCartDetailsSuccess(Cart[] carts) {
        mView.loadCartCount(carts.length);
    }

    @Override
    public void onGettingCartDetailsFail(String errorMsg, int loginError) {

    }
}
