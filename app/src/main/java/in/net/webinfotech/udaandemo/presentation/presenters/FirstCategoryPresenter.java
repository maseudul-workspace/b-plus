package in.net.webinfotech.udaandemo.presentation.presenters;

import android.widget.ImageView;

import in.net.webinfotech.udaandemo.presentation.ui.adapters.FirstCategoryAdapter;

/**
 * Created by Raj on 28-08-2019.
 */

public interface FirstCategoryPresenter {

    void fetchFirstCategory(int id);

    interface View {
        void loadData(FirstCategoryAdapter adapter);
        void goToSecondCategory(int id, String name, String image, ImageView imageView);
    }

}
