package in.net.webinfotech.udaandemo.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.udaandemo.presentation.presenters.SignUpPresenter;
import in.net.webinfotech.udaandemo.presentation.presenters.impl.SignUpPresenterImpl;
import in.net.webinfotech.udaandemo.threading.MainThreadImpl;

public class SignUpActivity extends AppCompatActivity implements SignUpPresenter.View {

    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_mobile)
    EditText editTextMobile;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    @BindView(R.id.edit_text_rp_password)
    EditText editTextRpPassword;
    ProgressDialog progressDialog;
    SignUpPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Sign Up");
        setUpProgressDialog();
        initialisePresenter();
    }

    public void initialisePresenter() {
        mPresenter = new SignUpPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.hide();
    }

    @Override
    public void goToLoginActivity() {
        finish();
    }

    @OnClick(R.id.btn_sign_up) void onSignUpClicked() {
        if (editTextName.getText().toString().trim().isEmpty() ||
                editTextEmail.getText().toString().isEmpty() ||
                editTextMobile.getText().toString().trim().isEmpty() ||
                editTextPassword.getText().toString().trim().isEmpty() ||
                editTextRpPassword.getText().toString().trim().isEmpty()
            ) {
            Toast.makeText(this, "Please fill all the fields", Toast.LENGTH_SHORT).show();
        } else if(editTextPassword.getText().toString().length() < 8) {
            Toast.makeText(this, "Password must be minumn 8 charcters in length", Toast.LENGTH_SHORT).show();
        } else if (!editTextPassword.getText().toString().equals(editTextRpPassword.getText().toString())) {
            Toast.makeText(this, "Password mismatch", Toast.LENGTH_SHORT).show();
        } else {
            mPresenter.signUp(
                    editTextName.getText().toString(),
                    editTextEmail.getText().toString(),
                    editTextMobile.getText().toString(),
                    editTextPassword.getText().toString(),
                    editTextRpPassword.getText().toString()
            );
            showLoader();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
