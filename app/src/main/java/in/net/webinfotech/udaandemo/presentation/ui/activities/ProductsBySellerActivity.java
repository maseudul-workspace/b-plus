package in.net.webinfotech.udaandemo.presentation.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.udaandemo.presentation.presenters.ProductsBySellerPresenter;
import in.net.webinfotech.udaandemo.presentation.presenters.impl.ProductsBySellerPresenterImpl;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.SellerProductListAdapter;
import in.net.webinfotech.udaandemo.threading.MainThreadImpl;

public class ProductsBySellerActivity extends AppCompatActivity implements ProductsBySellerPresenter.View{

    @BindView(R.id.recycler_view_product_by_seller_list)
    RecyclerView recyclerView;
    ProductsBySellerPresenterImpl mPresenter;
    int id;
    ProgressDialog progressDialog;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    Boolean isScrolling = false;
    int totalPage = 1;
    int pageNo = 1;
    LinearLayoutManager layoutManager;
    @BindView(R.id.pagination_progress_bar)
    View paginationProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list_by_seller);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Products By Seller");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialisePresenter();
        setUpProgressDialog();
        id = getIntent().getIntExtra("categoryId", 0);
        mPresenter.fetchProductsBySeller(id, pageNo, "refresh");
        showLoader();
    }

    public void initialisePresenter() {
        mPresenter = new ProductsBySellerPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadAdapter(SellerProductListAdapter adapter, int totalPage) {
        this.totalPage = totalPage;
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        showPaginationProgressBar();
                        mPresenter.fetchProductsBySeller(id, pageNo, "");
                    }
                }
            }
        });
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void goToProductList(int subcategoryId, int sellerId) {
        Intent intent = new Intent(this, ProductListActivity.class);
        intent.putExtra("subcategoryId", subcategoryId);
        intent.putExtra("sellerId", sellerId);
        startActivity(intent);
    }

    @Override
    public void goToProductDetails(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }

    @Override
    public void hidePaginationProgressBar() {
        paginationProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showPaginationProgressBar() {
        paginationProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
