package in.net.webinfotech.udaandemo.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.udaandemo.AndroidApplication;
import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.DeleteShippingAddressInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.FetchShippingAddressListInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.impl.DeleteShippingAddressInteractorImpl;
import in.net.webinfotech.udaandemo.domain.interactors.impl.FetchShippingAddressListInteractorImpl;
import in.net.webinfotech.udaandemo.domain.model.Address;
import in.net.webinfotech.udaandemo.domain.model.User;
import in.net.webinfotech.udaandemo.presentation.presenters.ShippingAddressPresenter;
import in.net.webinfotech.udaandemo.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.ShippingAddressAdapter;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;


public class ShippingAddressPresenterImpl extends AbstractPresenter implements ShippingAddressPresenter, FetchShippingAddressListInteractor.Callback, ShippingAddressAdapter.Callback, DeleteShippingAddressInteractor.Callback {

    Context mContext;
    ShippingAddressPresenter.View mView;
    ShippingAddressAdapter shippingAddressAdapter;
    AndroidApplication androidApplication;
    FetchShippingAddressListInteractorImpl fetchShippingAddressListInteractor;
    DeleteShippingAddressInteractorImpl deleteShippingAddressInteractor;

    public ShippingAddressPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchShippingAddress() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User userInfo = androidApplication.getUserInfo(mContext);
        fetchShippingAddressListInteractor = new FetchShippingAddressListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.userId, userInfo.apiToken);
        fetchShippingAddressListInteractor.execute();
    }

    @Override
    public void onGettingShippingAddressSuccess(Address[] addresses) {
        shippingAddressAdapter = new ShippingAddressAdapter(mContext, addresses, this);
        mView.loadAdapter(shippingAddressAdapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingShippingAddressFail(String errorMsg, int isLoginError) {
        mView.hideLoader();
        mView.hideAddressRecyclerView();
        if (isLoginError == 1) {
            mView.goToLoginActivity();
        }
    }

    @Override
    public void onRemoveClicked(int id) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User userInfo = androidApplication.getUserInfo(mContext);
        deleteShippingAddressInteractor = new DeleteShippingAddressInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, id);
        deleteShippingAddressInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onEditClicked(int id) {
        mView.goToAddressEditActivity(id);
    }

    @Override
    public void onAddressDeleteSuccess() {
        Toast.makeText(mContext, "Address deleted successfully", Toast.LENGTH_SHORT).show();
        fetchShippingAddress();
        mView.hideAddressRecyclerView();
    }

    @Override
    public void onAddressDeleteFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.goToLoginActivity();
        } else{
            Toast.makeText(mContext, "Failed to delete", Toast.LENGTH_SHORT).show();
        }
    }
}
