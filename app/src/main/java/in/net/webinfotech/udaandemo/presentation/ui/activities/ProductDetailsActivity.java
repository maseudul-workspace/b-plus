package in.net.webinfotech.udaandemo.presentation.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Paint;

import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.udaandemo.AndroidApplication;
import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.udaandemo.domain.model.Colors;
import in.net.webinfotech.udaandemo.domain.model.ProductDetails;
import in.net.webinfotech.udaandemo.domain.model.User;
import in.net.webinfotech.udaandemo.presentation.presenters.ProductDetailsPresenter;
import in.net.webinfotech.udaandemo.presentation.presenters.impl.ProductDetailsPresenterImpl;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.ColorsHorizontalAdapter;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.ProductImagesAdapter;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.ProductListHorizontalAdapter;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.SlidingImagesAdapter;
import in.net.webinfotech.udaandemo.presentation.ui.dialogs.ImageGalleryDialog;
import in.net.webinfotech.udaandemo.threading.MainThreadImpl;
import in.net.webinfotech.udaandemo.util.GlideHelper;

public class ProductDetailsActivity extends AppCompatActivity implements SlidingImagesAdapter.Callback, ProductDetailsPresenter.View, ProductImagesAdapter.Callback, ColorsHorizontalAdapter.Callback{

    @BindView(R.id.image_slider_viewpager)
    ViewPager imgSliderViewPager;
    @BindView(R.id.dots_indicator_layout)
    LinearLayout dotsLayout;
    private static int currentPage = 0;
    ImageGalleryDialog imageGalleryDialog;
    int sliderPosition = 0;
    @BindView(R.id.txt_view_product_name)
    TextView txtViewProductName;
    @BindView(R.id.txt_view_selling_price)
    TextView txtViewSellingPrice;
    @BindView(R.id.txt_view_price)
    TextView txtViewPrice;
    @BindView(R.id.txt_view_brand)
    TextView txtViewBrand;
    @BindView(R.id.txt_view_seller_name)
    TextView txtViewSellerName;
    @BindView(R.id.txt_view_short_desc)
    TextView txtViewShortDesc;
    @BindView(R.id.txt_view_desc)
    TextView txtViewDesc;
    @BindView(R.id.recycler_view_colors)
    RecyclerView recyclerViewColors;
    ProductDetailsPresenterImpl mPresenter;
    ColorsHorizontalAdapter colorsHorizontalAdapter;
    ProductImagesAdapter productImagesAdapter;
    int productId;
    Colors[] colors;
    @BindView(R.id.txt_view_color)
    TextView txtViewColor;
    @BindView(R.id.progress_bar_layout)
    View progressBar;
    @BindView(R.id.main_layout)
    View mainLayout;
    int minQty;
    @BindView(R.id.txt_view_qty)
    TextView txtViewQty;
    int currentQty;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    ProgressDialog progressDialog;
    int colorId;
    @BindView(R.id.txt_cart_count)
    TextView txtViewCartCount;
    @BindView(R.id.recycler_view_related_products)
    RecyclerView recyclerViewRelatedProducts;
    AndroidApplication androidApplication;
    @BindView(R.id.parent_layout)
    RelativeLayout relativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        ButterKnife.bind(this);
        productId = getIntent().getIntExtra("productId", 0);
//        setSliderViewPager();
//        imageGalleryDialog.setUpDialog();
        initialisePresenter();
        mPresenter.getProductDetails(productId);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUpProgressDialog();
    }

    public void initialisePresenter() {
        mPresenter = new ProductDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

//    public void setSliderViewPager(){
//        SlidingImagesAdapter slidingImagesAdapter = new SlidingImagesAdapter(this, headerSliders, this);
//        imgSliderViewPager.setAdapter(slidingImagesAdapter);
//        imgSliderViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//                sliderPosition = position;
//                prepareDotsIndicator(position);
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });
//    }

    public void prepareDotsIndicator(int sliderPosition, int size){
         if(dotsLayout.getChildCount() > 0){
             dotsLayout.removeAllViews();
         }

        ImageView dots[] = new ImageView[size];
        for(int i = 0; i < size; i++){
            dots[i] = new ImageView(this);
            if(i == sliderPosition){
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.active_dot);
            }else{
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.inactive_dot);
            }

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(15, 15);
            layoutParams.setMargins(5, 0, 5, 0);
            dotsLayout.addView(dots[i], layoutParams);

        }
    }

    @Override
    public void onImageClicked() {
        imageGalleryDialog.showDialog(sliderPosition);
    }

    @Override
    public void loadData(final ProductDetails productDetails) {
        mainLayout.setVisibility(View.VISIBLE);
        getSupportActionBar().setTitle(productDetails.product.name);
        txtViewProductName.setText(productDetails.product.name);
        txtViewSellingPrice.setText("Rs. " + productDetails.product.mrp);
        txtViewPrice.setText("Rs. " + productDetails.product.price);
        txtViewPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        txtViewBrand.setText(productDetails.product.brandName);
        minQty = productDetails.product.minimumOrderQty;
        txtViewQty.setText(Integer.toString(minQty));
        if (productDetails.sellers != null) {
            txtViewSellerName.setText(productDetails.sellers.sellerName);
        }
        txtViewShortDesc.setText(productDetails.product.shortDescription);
        txtViewDesc.setText(productDetails.product.longDescription);

        if (productDetails.productColors == null || productDetails.productColors.length == 0) {
            txtViewColor.setVisibility(View.GONE);
            recyclerViewColors.setVisibility(View.GONE);
        } else {
            colors = productDetails.productColors;
            colors[0].isSelected = true;
            colorId = colors[0].colorId;
            colorsHorizontalAdapter = new ColorsHorizontalAdapter(this, colors, this);
            recyclerViewColors.setAdapter(colorsHorizontalAdapter);
            recyclerViewColors.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        }

        productImagesAdapter = new ProductImagesAdapter(this, productDetails.productImages, this);
        imgSliderViewPager.setAdapter(productImagesAdapter);
        prepareDotsIndicator(0, productDetails.productImages.length);
        imgSliderViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                sliderPosition = position;
                prepareDotsIndicator(position, productDetails.productImages.length);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        currentQty = minQty;
    }

    @Override
    public void showLoader() {

    }

    @Override
    public void hideLoader() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showProgressDialog() {
        progressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        progressDialog.hide();
    }

    @Override
    public void loadCartCount(int count) {
        if (count == 0) {
            txtViewCartCount.setText("");
        } else {
            txtViewCartCount.setText(Integer.toString(count));
        }
    }

    @Override
    public void loadRelatedProducts(ProductListHorizontalAdapter adapter) {
        recyclerViewRelatedProducts.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        recyclerViewRelatedProducts.setAdapter(adapter);
    }

    @Override
    public void onProductClicked(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }

    @Override
    public void goToLoginActivity() {
        Toast.makeText(this, "Your session expired !! Please login again", Toast.LENGTH_LONG).show();
        Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(loginIntent);
    }

    @Override
    public void onImageClicked(int id) {

    }

    @Override
    public void onColorSelect(int id) {
        for (int i = 0; i < colors.length; i++) {
            if (colors[i].colorId == id) {
                colors[i].isSelected = true;
                colorId = colors[i].colorId;
            } else {
                colors[i].isSelected = false;
            }
        }
        colorsHorizontalAdapter.updateColors(colors);
    }

    @OnClick(R.id.img_view_minus) void qtyMinusClicked() {
        if (currentQty > minQty) {
            currentQty--;
            txtViewQty.setText(Integer.toString(currentQty));
        }
    }

    @OnClick(R.id.img_view_plus) void qrtPlusClicked() {
        currentQty++;
        txtViewQty.setText(Integer.toString(currentQty));
    }

    @OnClick(R.id.layout_add_to_cart) void addToCart() {
        if (isLoggedIn()) {
            mPresenter.addToCart(productId, colorId, currentQty);
            showProgressDialog();
        } else {
            showLogInSnackbar();
        }
    }

    @OnClick({R.id.layout_cart}) void onLayoutCartClicked() {
        if (isLoggedIn()) {
            Intent intent = new Intent(this, CartActivity.class);
            startActivity(intent);
        } else {
            showLogInSnackbar();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchCartCount();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showLogInSnackbar(){
        Snackbar snackbar = Snackbar.make(relativeLayout, "You must be logged in", Snackbar.LENGTH_LONG);
        snackbar.setAction("Log In", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(loginIntent);
                finish();
            }
        });
        snackbar.show();
    }

    public boolean isLoggedIn() {
        androidApplication = (AndroidApplication) getApplicationContext();
        User user = androidApplication.getUserInfo(this);
        if (user == null) {
            return false;
        } else {
            return true;
        }
    }

}
