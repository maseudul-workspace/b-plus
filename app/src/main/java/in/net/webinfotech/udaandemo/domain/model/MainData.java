package in.net.webinfotech.udaandemo.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 28-08-2019.
 */

public class MainData {

    @SerializedName("main_category_list")
    @Expose
    public MainCategory[] mainCategories;

    @SerializedName("sliders")
    @Expose
    public MainSliders[] mainSliders;

    @SerializedName("popular_products")
    @Expose
    public Product[] popularProducts;

    @SerializedName("new_arrivals")
    @Expose
    public Product[] newProducts;

    @SerializedName("promotional_image")
    @Expose
    public ProductImages[] promotionalImages;

}
