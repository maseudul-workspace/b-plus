package in.net.webinfotech.udaandemo.presentation.ui.adapters;

import android.content.Context;
import android.graphics.Paint;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.model.Product;
import in.net.webinfotech.udaandemo.util.GlideHelper;

/**
 * Created by Raj on 30-04-2019.
 */

public class ProductListUnderSellerAdapter extends RecyclerView.Adapter<ProductListUnderSellerAdapter.ViewHolder>{

    public interface Callback {
        void onProductClicked(int productId);
    }

    Context mContext;
    Product[] products;
    Callback mCallback;

    public ProductListUnderSellerAdapter(Context mContext, Product[] products, Callback callback) {
        this.mContext = mContext;
        this.products = products;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_products_under_seller_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        GlideHelper.setImageView(mContext, holder.imgViewProductPoster, mContext.getResources().getString(R.string.api_url) + "product/image/" +  products[position].mainImage);
        holder.txtViewProductName.setText(products[position].name);
        holder.txtViewSellingPrice.setText("Rs. " + products[position].price);
        if (products[position].mrp != null) {
            holder.txtViewSellingPrice.setText("Rs. " + products[position].mrp);
        }
        holder.txtViewProductPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        holder.txtViewMinQty.setText("Min qty: " + products[position].minimumOrderQty);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onProductClicked(products[position].id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return products.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_product_poster)
        ImageView imgViewProductPoster;
        @BindView(R.id.txt_view_product_name)
        TextView txtViewProductName;
        @BindView(R.id.txt_view_price)
        TextView txtViewProductPrice;
        @BindView(R.id.txt_view_selling_price)
        TextView txtViewSellingPrice;
        @BindView(R.id.txt_view_min_qty)
        TextView txtViewMinQty;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
