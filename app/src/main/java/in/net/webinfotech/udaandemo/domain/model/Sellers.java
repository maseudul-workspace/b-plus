package in.net.webinfotech.udaandemo.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 30-08-2019.
 */

public class Sellers {

    @SerializedName("seller_id")
    @Expose
    public int sellerId;

    @SerializedName("seller_name")
    @Expose
    public String sellerName;

}
