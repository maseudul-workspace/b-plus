package in.net.webinfotech.udaandemo.domain.interactors;

public interface UpdateUserProfileInteractor {
    interface Callback {
        void onUserProfileUpdateSuccess();
        void onUserProfileUpdateFail(String errorMsg, int loginError);
    }
}
