package in.net.webinfotech.udaandemo.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.model.ProductImages;
import in.net.webinfotech.udaandemo.util.GlideHelper;

/**
 * Created by Raj on 03-09-2019.
 */

public class ProductImagesAdapter extends PagerAdapter {

    public interface Callback {
        void onImageClicked(int id);
    }

    Context mContext;
    ProductImages[] productImages;
    Callback mCallback;

    public ProductImagesAdapter(Context mContext, ProductImages[] productImages, Callback callback) {
        this.mContext = mContext;
        this.productImages = productImages;
        mCallback = callback;
    }

    @Override
    public int getCount() {
        return productImages.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView imageView = new ImageView(mContext);
        GlideHelper.setImageView(mContext, imageView, mContext.getResources().getString(R.string.api_url) + "product/image/" + productImages[position].image);
        container.addView(imageView);
        return imageView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

}
