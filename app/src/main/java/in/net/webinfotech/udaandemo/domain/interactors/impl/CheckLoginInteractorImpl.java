package in.net.webinfotech.udaandemo.domain.interactors.impl;

import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.CheckLoginInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.udaandemo.domain.model.User;
import in.net.webinfotech.udaandemo.domain.model.UserWrapper;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;

public class CheckLoginInteractorImpl extends AbstractInteractor implements CheckLoginInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String email;
    String password;

    public CheckLoginInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String email, String password) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.email = email;
        this.password = password;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCheckLoginFail(errorMsg);
            }
        });
    }

    private void postMessage(User user){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCheckLoginSuccess(user);
            }
        });
    }

    @Override
    public void run() {
        final UserWrapper userWrapper = mRepository.checkLogin(email, password);
        if (userWrapper == null) {
            notifyError("Something went wrong");
        } else if (!userWrapper.status) {
            notifyError(userWrapper.message);
        } else {
            postMessage(userWrapper.user);
        }
    }
}
