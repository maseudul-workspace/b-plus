package in.net.webinfotech.udaandemo.presentation.ui.adapters;

import android.content.Context;
import android.graphics.Paint;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.model.Product;
import in.net.webinfotech.udaandemo.util.GlideHelper;

/**
 * Created by Raj on 02-09-2019.
 */

public class ProductListVerticalAdapter extends RecyclerView.Adapter<ProductListVerticalAdapter.ViewHolder> {

    public interface Callback {
        void onProductClicked(int productId);
    }

    Context mContext;
    Product[] products;
    Callback mCallback;

    public ProductListVerticalAdapter(Context mContext, Product[] products, Callback callback) {
        this.mContext = mContext;
        this.products = products;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_product_list_vertical, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.txtViewProductName.setText(products[i].name);
        viewHolder.txtViewSellingPrice.setText("Rs. " + products[i].price);
        if (products[i].mrp != null) {
            viewHolder.txtViewPrice.setText("Rs. " + products[i].mrp);
        }
        GlideHelper.setImageView(mContext, viewHolder.imgViewProduct, mContext.getResources().getString(R.string.api_url) + "product/image/thumb/" + products[i].mainImage);
        viewHolder.txtViewPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        viewHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onProductClicked(products[i].id);
            }
        });
        viewHolder.txtViewBrand.setText(products[i].brandName);
        viewHolder.txtViewTag.setText(products[i].tagNmae);
    }

    @Override
    public int getItemCount() {
        return products.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_product)
        ImageView imgViewProduct;
        @BindView(R.id.txt_view_product_name)
        TextView txtViewProductName;
        @BindView(R.id.txt_view_selling_price)
        TextView txtViewSellingPrice;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;
        @BindView(R.id.main_layout)
        View mainLayout;
        @BindView(R.id.txt_view_brand)
        TextView txtViewBrand;
        @BindView(R.id.txt_view_tag)
        TextView txtViewTag;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataSet(Product[] products) {
        this.products = products;
        notifyDataSetChanged();
    }

}
