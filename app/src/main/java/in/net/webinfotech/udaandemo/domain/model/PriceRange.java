package in.net.webinfotech.udaandemo.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 30-08-2019.
 */

public class PriceRange {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("amount")
    @Expose
    public double amount;

    @SerializedName("payment_method")
    @Expose
    public double payment_method;

}