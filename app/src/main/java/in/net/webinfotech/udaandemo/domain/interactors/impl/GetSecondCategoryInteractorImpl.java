package in.net.webinfotech.udaandemo.domain.interactors.impl;

import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.GetSecondCategoryInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.udaandemo.domain.model.SecondCategory;
import in.net.webinfotech.udaandemo.domain.model.SecondCategoryWrapper;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;

/**
 * Created by Raj on 29-08-2019.
 */

public class GetSecondCategoryInteractorImpl extends AbstractInteractor implements GetSecondCategoryInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    int id;

    public GetSecondCategoryInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, int id) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.id = id;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingSecondCategoriesFail(errorMsg);
            }
        });
    }

    private void postMessage(final SecondCategory[] secondCategories){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingSecondCategoriesSuccess(secondCategories);
            }
        });
    }


    @Override
    public void run() {
        final SecondCategoryWrapper secondCategoryWrapper = mRepository.fetchSecondCategory(id);
        if (secondCategoryWrapper == null) {
            notifyError("Something went wrong");
        } else if (!secondCategoryWrapper.status) {
            notifyError(secondCategoryWrapper.message);
        } else {
            postMessage(secondCategoryWrapper.secondCategories);
        }
    }
}
