package in.net.webinfotech.udaandemo.domain.interactors.impl;

import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.FetchUserInfoInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.udaandemo.domain.model.User;
import in.net.webinfotech.udaandemo.domain.model.UserDetails;
import in.net.webinfotech.udaandemo.domain.model.UserDetailsWrapper;
import in.net.webinfotech.udaandemo.domain.model.UserWrapper;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;

public class FetchUserInfoInteractorImpl extends AbstractInteractor implements FetchUserInfoInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiToken;

    public FetchUserInfoInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int userId, String apiToken) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiToken = apiToken;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingUserInfoFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(UserDetails user){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingUserInfoSuccess(user);
            }
        });
    }

    @Override
    public void run() {
        final UserDetailsWrapper userWrapper = mRepository.fetchUserInfo(userId, apiToken);
        if (userWrapper == null) {
            notifyError("Something went wrong", 0);
        } else if (!userWrapper.status) {
            notifyError(userWrapper.message, userWrapper.login_error);
        } else {
            postMessage(userWrapper.userDetails);
        }
    }
}
