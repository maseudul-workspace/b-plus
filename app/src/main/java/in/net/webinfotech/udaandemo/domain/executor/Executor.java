package in.net.webinfotech.udaandemo.domain.executor;

import in.net.webinfotech.udaandemo.domain.interactors.base.AbstractInteractor;

/**
 * Created by Raj on 24-04-2019.
 */

public interface Executor {
    /**
     * This method should call the interactor's run method and thus start the interactor. This should be called
     * on a background thread as interactors might do lengthy operations.
     *
     * @param interactor The interactor to run.
     */
    void execute(final AbstractInteractor interactor);
}
