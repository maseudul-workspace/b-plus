package in.net.webinfotech.udaandemo.presentation.presenters;

import java.util.ArrayList;

import in.net.webinfotech.udaandemo.domain.model.FilterData;
import in.net.webinfotech.udaandemo.domain.model.ProductListWithFilters;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.ProductListVerticalAdapter;

/**
 * Created by Raj on 02-09-2019.
 */

public interface ProductListPresenter {

    void fetchProductListWithFilters(int subcategoryId, int sellerId, int pageNo, String type);
    void fetchProductListWithoutFilters(int secondCategory,
                                        ArrayList<Integer> sellerIds,
                                        ArrayList<Integer> brandIds,
                                        ArrayList<Integer> colorIds,
                                        int minPrice,
                                        int maxPrice,
                                        String sortBy,
                                        int page,
                                        String type
                                        );
    interface View {
        void loadFilters(FilterData filterData);
        void loadAdapter(ProductListVerticalAdapter adapter, int totalPage);
        void goToProductDetails(int productId);
        void showLoader();
        void hideLoader();
        void hidePaginationProgressBar();
    }
}
