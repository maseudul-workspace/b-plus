package in.net.webinfotech.udaandemo.domain.interactors;

import in.net.webinfotech.udaandemo.domain.model.SellerListWrapper;
import in.net.webinfotech.udaandemo.domain.model.SellersList;

/**
 * Created by Raj on 29-08-2019.
 */

public interface GetProductsBySellerInteractor {
    interface Callback {
        void onGettingProductsListSuccess(SellersList[] sellersLists, int totalPage);
        void onGettingProductsListFail(String errorMsg);
    }
}
