package in.net.webinfotech.udaandemo.domain.interactors;

import in.net.webinfotech.udaandemo.domain.model.Address;

public interface GetAddressDetailsInteractor {
    interface Callback {
        void onGettingAddressDetailsSuccess(Address address);
        void onGettingAddressDetailsFail(String errorMsg, int loginError);
    }
}
