package in.net.webinfotech.udaandemo.presentation.ui.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.model.Order;
import in.net.webinfotech.udaandemo.util.GlideHelper;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.ViewHolder> {

    public interface Callback {
        void onCancelClicked(int id);
    }

    Context mContext;
    Order[] orders;
    Callback mCallback;

    public OrderListAdapter(Context mContext, Order[] orders, Callback mCallback) {
        this.mContext = mContext;
        this.orders = orders;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_orders, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewOrderId.setText(Integer.toString(orders[position].id));
        holder.txtViewOrderTotalPrice.setText(Double.toString(orders[position].total));
        holder.txtViewQuantity.setText(Integer.toString(orders[position].quantity));
        holder.txtViewProductPrice.setText(Double.toString(orders[position].price));
        GlideHelper.setImageView(mContext, holder.imgViewProductImage, mContext.getResources().getString(R.string.api_url) + "product/image/thumb/" + orders[position].image);
        if (orders[position].status == 1) {
            holder.txtViewOrderStatus.setText("Processing");
            holder.txtViewOrderStatus.setTextColor(mContext.getResources().getColor(R.color.blue2));
            holder.layoutBlue.setVisibility(View.VISIBLE);
            holder.layoutGreen.setVisibility(View.GONE);
            holder.layoutRed.setVisibility(View.GONE);
            holder.txtViewOrderCancel.setVisibility(View.VISIBLE);
        } else if (orders[position].status == 2) {
            holder.txtViewOrderStatus.setText("Processed");
            holder.txtViewOrderStatus.setTextColor(mContext.getResources().getColor(R.color.green2));
            holder.layoutBlue.setVisibility(View.GONE);
            holder.layoutGreen.setVisibility(View.VISIBLE);
            holder.layoutRed.setVisibility(View.GONE);
            holder.txtViewOrderCancel.setVisibility(View.GONE);
        } else {
            holder.txtViewOrderStatus.setText("Cancelled");
            holder.txtViewOrderStatus.setTextColor(mContext.getResources().getColor(R.color.red2));
            holder.layoutBlue.setVisibility(View.GONE);
            holder.layoutGreen.setVisibility(View.GONE);
            holder.layoutRed.setVisibility(View.VISIBLE);
            holder.txtViewOrderCancel.setVisibility(View.GONE);
        }
        if (orders[position].paymentMethod == 1) {
            holder.txtViewPaymentMethod.setText("Cash On Delivery");
        } else {
            holder.txtViewPaymentMethod.setText("Online");
        }
        holder.txtViewProductName.setText(orders[position].productName);
        holder.txtViewProductBrand.setText(orders[position].brandName);
        String date = changeDateFormat(orders[position].orderDate);
        holder.txtViewDate.setText(date);
        holder.txtViewOrderCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Are You Sure ?");
                builder.setMessage("You are about to cancel a order. Do you really want to proceed ?");
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.onCancelClicked(orders[position].id);
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                builder.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return orders.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_product_name)
        TextView txtViewProductName;
        @BindView(R.id.img_view_product_image)
        ImageView imgViewProductImage;
        @BindView(R.id.txt_view_product_brand)
        TextView txtViewProductBrand;
        @BindView(R.id.txt_view_product_price)
        TextView txtViewProductPrice;
        @BindView(R.id.main_layout)
        View mainLayout;
        @BindView(R.id.txt_view_quantity)
        TextView txtViewQuantity;
        @BindView(R.id.txt_view_order_id)
        TextView txtViewOrderId;
        @BindView(R.id.txt_view_cancel_order)
        TextView txtViewOrderCancel;
        @BindView(R.id.txt_view_order_total_price)
        TextView txtViewOrderTotalPrice;
        @BindView(R.id.txt_view_order_status)
        TextView txtViewOrderStatus;
        @BindView(R.id.layout_blue)
        View layoutBlue;
        @BindView(R.id.layout_green)
        View layoutGreen;
        @BindView(R.id.layout_red)
        View layoutRed;
        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_payment_method)
        TextView txtViewPaymentMethod;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public String changeDateFormat(String strCurrentDate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        Date newDate = null;
        try {
            newDate = format.parse(strCurrentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        format = new SimpleDateFormat("EEE, dd MMM yyyy");
        String date = format.format(newDate);
        return date;
    }

}
