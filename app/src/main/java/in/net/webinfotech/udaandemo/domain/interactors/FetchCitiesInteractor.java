package in.net.webinfotech.udaandemo.domain.interactors;


import in.net.webinfotech.udaandemo.domain.model.Cities;

public interface FetchCitiesInteractor {
    interface Callback {
        void onGettingCitiesSuccess(Cities[] cities);
        void onGettingCitiesFail();
    }
}
