package in.net.webinfotech.udaandemo.domain.model.Testing;

import java.util.ArrayList;

/**
 * Created by Raj on 30-04-2019.
 */

public class ProductListBySeller {
    public String sellerName;
    public String sellerImage;
    public String address;
    public ArrayList<ProductsList> productsLists;

    public ProductListBySeller(String sellerName, String sellerImage, String address, ArrayList<ProductsList> productsLists) {
        this.sellerName = sellerName;
        this.sellerImage = sellerImage;
        this.address = address;
        this.productsLists = productsLists;
    }
}
