package in.net.webinfotech.udaandemo.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderPlaceData {

    @SerializedName("id")
    @Expose
    public int orderId;

    @SerializedName("amount")
    @Expose
    public Double amount;

}
