package in.net.webinfotech.udaandemo;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import in.net.webinfotech.udaandemo.domain.model.User;

/**
 * Created by Raj on 22-07-2019.
 */

public class AndroidApplication extends Application {

    User userInfo;
    int walletStatus;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public void setUserInfo(Context context, User userInfo){
        this.userInfo = userInfo;
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.KEY_BPLUS_APP_PREFERENCES), Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(userInfo != null) {
            editor.putString(getString(R.string.KEY_USER_DETAILS), new Gson().toJson(userInfo));
        } else {
            editor.putString(getString(R.string.KEY_USER_DETAILS), "");
        }

        editor.commit();
    }

    public User getUserInfo(Context context){
        User user;
        if(userInfo != null){
            user = userInfo;
        }else{
            SharedPreferences sharedPref = context.getSharedPreferences(
                    getString(R.string.KEY_BPLUS_APP_PREFERENCES), Context.MODE_PRIVATE);
            String userInfoJson = sharedPref.getString(getString(R.string.KEY_USER_DETAILS),"");
            if(userInfoJson.isEmpty()){
                user = null;
            }else{
                try {
                    user = new Gson().fromJson(userInfoJson, User.class);
                    this.userInfo = user;
                }catch (Exception e){
                    user = null;
                }
            }
        }
        return user;
    }

}
