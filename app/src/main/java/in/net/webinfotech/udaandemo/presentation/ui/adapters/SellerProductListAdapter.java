package in.net.webinfotech.udaandemo.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.model.SellersList;
import in.net.webinfotech.udaandemo.util.GlideHelper;

/**
 * Created by Raj on 30-04-2019.
 */

public class SellerProductListAdapter extends RecyclerView.Adapter<SellerProductListAdapter.ViewHolder> implements ProductListUnderSellerAdapter.Callback{

    @Override
    public void onProductClicked(int productId) {
        mCallback.onProductClicked(productId);
    }

    public interface Callback {
        void onSeeAllClicked(int subcategoryId, int sellerId);
        void onProductClicked(int productId);
    }

    Context mContext;
    SellersList[] sellersLists;
    Callback mCallback;

    public SellerProductListAdapter(Context mContext, SellersList[] sellersLists, Callback callback) {
        this.mContext = mContext;
        this.sellersLists = sellersLists;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_seller_product_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        ProductListUnderSellerAdapter adapter = new ProductListUnderSellerAdapter(mContext, sellersLists[position].products, this);
        holder.recyclerView.setAdapter(adapter);
        holder.recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        GlideHelper.setImageViewCustomRoundedCornersDrawable(mContext, holder.imgViewSellerPic, R.drawable.user_color_accent, 100);
        holder.txtViewSellerName.setText(sellersLists[position].sellerName);
        holder.txtViewSellerAddress.setText(sellersLists[position].sellerState + ", " + sellersLists[position].sellerCity);
        holder.txtViewSeeAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onSeeAllClicked(sellersLists[position].secondCategoryId, sellersLists[position].sellerId);
            }
        });
    }

    @Override
    public int getItemCount() {
        return sellersLists.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.recycler_view_product_list_under_seller)
        RecyclerView recyclerView;
        @BindView(R.id.img_view_seller_pic)
        ImageView imgViewSellerPic;
        @BindView(R.id.txt_view_seller_name)
        TextView txtViewSellerName;
        @BindView(R.id.txt_view_seller_address)
        TextView txtViewSellerAddress;
        @BindView(R.id.txt_view_see_all)
        TextView txtViewSeeAll;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataSet(SellersList[] sellersLists) {
        this.sellersLists = sellersLists;
        notifyDataSetChanged();
    }

}
