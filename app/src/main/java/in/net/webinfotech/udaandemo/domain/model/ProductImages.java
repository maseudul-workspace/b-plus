package in.net.webinfotech.udaandemo.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 03-09-2019.
 */

public class ProductImages {

    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("id")
    @Expose
    public int id;

}
