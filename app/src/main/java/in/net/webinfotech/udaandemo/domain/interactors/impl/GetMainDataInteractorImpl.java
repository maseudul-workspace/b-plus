package in.net.webinfotech.udaandemo.domain.interactors.impl;

import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.GetMainDataInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.udaandemo.domain.model.MainData;
import in.net.webinfotech.udaandemo.domain.model.MainDataWrapper;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;

/**
 * Created by Raj on 28-08-2019.
 */

public class GetMainDataInteractorImpl extends AbstractInteractor implements GetMainDataInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;

    public GetMainDataInteractorImpl(Executor threadExecutor,
                                     MainThread mainThread,
                                     Callback callback,
                                     AppRepositoryImpl repository
                                     ) {
        super(threadExecutor, mainThread);
        mCallback = callback;
        mRepository = repository;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingMainDataFail();
            }
        });
    }

    private void postMessage(final MainData mainData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingMainDataSuccess(mainData);
            }
        });
    }

    @Override
    public void run() {
        MainDataWrapper mainDataWrapper = mRepository.fetchMainData();
        if (mainDataWrapper == null) {
            notifyError();
        } else if (!mainDataWrapper.status) {
            notifyError();
        } else {
            postMessage(mainDataWrapper.mainData);
        }
    }
}
