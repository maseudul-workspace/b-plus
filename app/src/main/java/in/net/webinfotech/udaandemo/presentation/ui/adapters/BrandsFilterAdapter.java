package in.net.webinfotech.udaandemo.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.model.Brands;

/**
 * Created by Raj on 02-09-2019.
 */

public class BrandsFilterAdapter extends RecyclerView.Adapter<BrandsFilterAdapter.ViewHolder> {

    public interface Callback {
        void onBrandsSelect(int brandId);
        void onBrandsNotSelect(int brandId);
    }

    Context mContext;
    Brands[] brands;
    Callback mCallback;

    public BrandsFilterAdapter(Context mContext, Brands[] brands, Callback callback) {
        this.mContext = mContext;
        this.brands = brands;
        this.mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_filter_by, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.checkBox.setText(brands[i].brandName);
        viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mCallback.onBrandsSelect(brands[i].brandId);
                } else {
                    mCallback.onBrandsNotSelect(brands[i].brandId);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return brands.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.recycler_view_filter_by_checkbox)
        CheckBox checkBox;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
