package in.net.webinfotech.udaandemo.presentation.ui.activities;

import android.content.Intent;
import android.os.Handler;
import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.viewpager.widget.ViewPager;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;
import in.net.webinfotech.udaandemo.AndroidApplication;
import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.udaandemo.domain.model.MainCategory;
import in.net.webinfotech.udaandemo.domain.model.MainData;
import in.net.webinfotech.udaandemo.domain.model.MainSliders;
import in.net.webinfotech.udaandemo.domain.model.Product;
import in.net.webinfotech.udaandemo.domain.model.ProductImages;
import in.net.webinfotech.udaandemo.domain.model.Testing.HomeHeaderSliders;
import in.net.webinfotech.udaandemo.domain.model.User;
import in.net.webinfotech.udaandemo.presentation.presenters.MainPresenter;
import in.net.webinfotech.udaandemo.presentation.presenters.impl.MainPresenterImpl;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.CategoryHorizontalAdapter;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.ProductListHorizontalAdapter;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.SlidingImagesAdapter;
import in.net.webinfotech.udaandemo.threading.MainThreadImpl;
import in.net.webinfotech.udaandemo.util.GlideHelper;

public class MainActivity extends BaseActivity implements SlidingImagesAdapter.Callback, MainPresenter.View, CategoryHorizontalAdapter.Callback, ProductListHorizontalAdapter.Callback{


    @BindView(R.id.image_slider_viewpager)
    ViewPager imgSliderViewPager;
    @BindView(R.id.recycler_view_categories)
    RecyclerView recyclerViewCategories;
    @BindView(R.id.recycler_view_best_selling)
    RecyclerView recyclerViewBestSelling;
    @BindView(R.id.recycler_view_newest_arrival)
    RecyclerView recyclerViewNewestArrival;
    ArrayList<HomeHeaderSliders> headerSliders;
    LinearLayoutManager layoutManager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    @BindView(R.id.navigation)
    BottomNavigationView bottomNavigationView;
    MainPresenterImpl mPresenter;
    @BindView(R.id.parent_layout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.img_promotion1)
    ImageView imgViewPromotiionImage1;
    @BindView(R.id.img_promotion2)
    ImageView imgViewPromotiionImage2;
    @BindView(R.id.img_promotion3)
    ImageView imgViewPromotiionImage3;
    @BindView(R.id.img_promotion4)
    ImageView imgViewPromotiionImage4;
    AndroidApplication androidApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_main);
        headerSliders = new ArrayList<>();
        setBottomNavigationView();
        initialisePresenter();
        mPresenter.fetchHomeData();
    }

    public void initialisePresenter() {
        mPresenter = new MainPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setBottomNavigationView(){
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_profile:
                        if (isLoggedIn()) {
                            Intent userProfileIntent = new Intent(getApplicationContext(), UserProfileActivity.class);
                            startActivity(userProfileIntent);
                        } else {
                            showLogInSnackbar();
                        }
                        break;
                    case R.id.action_order:
                        if (isLoggedIn()) {
                            Intent orderIntent = new Intent(getApplicationContext(), OrderListActivity.class);
                            startActivity(orderIntent);
                        } else {
                            showLogInSnackbar();
                        }

                        break;
                }
                return false;
            }
        });

    }

   public void setSliderViewPager(MainSliders[] mainSliders){
       SlidingImagesAdapter slidingImagesAdapter = new SlidingImagesAdapter(this, mainSliders, this);
       imgSliderViewPager.setAdapter(slidingImagesAdapter);

       // Auto start of viewpager
       final Handler handler = new Handler();
       final Runnable Update = new Runnable() {
           public void run() {
               if (currentPage == headerSliders.size()) {
                   currentPage = 0;
               }
               imgSliderViewPager.setCurrentItem(currentPage++, true);
           }
       };
       Timer swipeTimer = new Timer();
       swipeTimer.schedule(new TimerTask() {
           @Override
           public void run() {
               handler.post(Update);
           }
       }, 250, 2500);
   }

   public void setMainCategories(MainCategory[] mainCategories) {
       CategoryHorizontalAdapter adapter = new CategoryHorizontalAdapter(this, mainCategories, this);
       recyclerViewCategories.setAdapter(adapter);
       recyclerViewCategories.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
   }

   public void setProductsListsBestSellingAdapter(Product[] products){
       ProductListHorizontalAdapter adapter = new ProductListHorizontalAdapter(this, products, this);
       recyclerViewBestSelling.setAdapter(adapter);
       recyclerViewBestSelling.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

   }

    public void setProductsListsNewestArrivalAdapter(Product[] products){
        ProductListHorizontalAdapter adapter = new ProductListHorizontalAdapter(this, products, this);
        recyclerViewNewestArrival.setAdapter(adapter);
        recyclerViewNewestArrival.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

    }

    @OnClick(R.id.txt_view_best_selling_view_more) void onBestSellingViewMoreClicked(){
        Intent productListIntent = new Intent(this, ProductsBySellerActivity.class);
        startActivity(productListIntent);
    }

    @Override
    public void onImageClicked() {

    }

    @Override
    public void loadMainData(MainData mainData) {
        setSliderViewPager(mainData.mainSliders);
        setProductsListsBestSellingAdapter(mainData.popularProducts);
        setProductsListsNewestArrivalAdapter(mainData.newProducts);
        setMainCategories(mainData.mainCategories);
        ProductImages[] promotionalImages = mainData.promotionalImages;
        GlideHelper.setImageView(this, imgViewPromotiionImage1, getResources().getString(R.string.api_url) + "slider/image/" + promotionalImages[0].image);
        GlideHelper.setImageView(this, imgViewPromotiionImage2, getResources().getString(R.string.api_url) + "slider/image/" + promotionalImages[1].image);
        GlideHelper.setImageView(this, imgViewPromotiionImage3, getResources().getString(R.string.api_url) + "slider/image/" + promotionalImages[2].image);
        GlideHelper.setImageView(this, imgViewPromotiionImage4, getResources().getString(R.string.api_url) + "slider/image/" + promotionalImages[3].image);
    }

    @Override
    public void loadCartCount(int count) {
        loadBaseCartCount(count);
    }

    @Override
    public void onCategoryClicked(int id, String name) {
        Intent intent = new Intent(this, FirstCategoryActivity.class);
        intent.putExtra("mainCategoryId", id);
        intent.putExtra("mainCategoryName", name);
        startActivity(intent);
    }

    @Override
    public void onProductClicked(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchCartItems();
    }

    public void showLogInSnackbar(){
        Toast.makeText(this, "Please log in ", Toast.LENGTH_LONG).show();
    }

    public boolean isLoggedIn() {
        androidApplication = (AndroidApplication) getApplicationContext();
        User user = androidApplication.getUserInfo(this);
        if (user == null) {
            return false;
        } else {
            return true;
        }
    }

}
