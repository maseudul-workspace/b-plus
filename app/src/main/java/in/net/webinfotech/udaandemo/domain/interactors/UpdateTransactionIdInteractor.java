package in.net.webinfotech.udaandemo.domain.interactors;

public interface UpdateTransactionIdInteractor {
    interface Callback {
        void onUpdateTransactionIdSuccess();
        void onUpdateTransactionIdFail(String errorMsg, int loginError);
    }
}
