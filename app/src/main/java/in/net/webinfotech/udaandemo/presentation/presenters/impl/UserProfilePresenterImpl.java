package in.net.webinfotech.udaandemo.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.udaandemo.AndroidApplication;
import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.FetchCitiesInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.FetchStateListInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.FetchUserInfoInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.UpdateUserProfileInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.impl.FetchCitiesInteractorImpl;
import in.net.webinfotech.udaandemo.domain.interactors.impl.FetchStateListInteractorImpl;
import in.net.webinfotech.udaandemo.domain.interactors.impl.FetchUserInfoInteractorImpl;
import in.net.webinfotech.udaandemo.domain.interactors.impl.UpdateTransactionIdInteractorImpl;
import in.net.webinfotech.udaandemo.domain.interactors.impl.UpdateUserProfileInteractorImpl;
import in.net.webinfotech.udaandemo.domain.model.Cities;
import in.net.webinfotech.udaandemo.domain.model.StateList;
import in.net.webinfotech.udaandemo.domain.model.User;
import in.net.webinfotech.udaandemo.domain.model.UserDetails;
import in.net.webinfotech.udaandemo.presentation.presenters.UserProfilePresenter;
import in.net.webinfotech.udaandemo.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.CityListDialogAdapter;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.StateListDialogAdapter;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;

public class UserProfilePresenterImpl extends AbstractPresenter implements  UserProfilePresenter,
                                                                            FetchUserInfoInteractor.Callback,
                                                                            FetchStateListInteractor.Callback,
                                                                            StateListDialogAdapter.Callback,
                                                                            FetchCitiesInteractor.Callback,
                                                                            CityListDialogAdapter.Callback,
                                                                            UpdateUserProfileInteractor.Callback
{

    Context mContext;
    UserProfilePresenter.View mView;
    FetchUserInfoInteractorImpl fetchUserInfoInteractor;
    AndroidApplication androidApplication;
    FetchStateListInteractorImpl fetchStateListInteractor;
    StateListDialogAdapter stateListDialogAdapter;
    FetchCitiesInteractorImpl fetchCitiesInteractor;
    int stateId = 0;
    CityListDialogAdapter cityListDialogAdapter;
    int cityId = 0;
    UpdateUserProfileInteractorImpl updateUserProfileInteractor;

    public UserProfilePresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchStateList() {
        fetchStateListInteractor = new FetchStateListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this);
        fetchStateListInteractor.execute();
    }

    @Override
    public void fetchCityList() {
        fetchCitiesInteractor = new FetchCitiesInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, stateId);
        fetchCitiesInteractor.execute();
    }

    @Override
    public void fetchUserProfile() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        fetchUserInfoInteractor = new FetchUserInfoInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.userId, user.apiToken);
        fetchUserInfoInteractor.execute();
    }

    @Override
    public void updateUserProfile(String gender, String name, String mobile, String pin, String address) {
        if (cityId == 0 || stateId == 0) {
            Toast.makeText(mContext, "State or City is missing", Toast.LENGTH_SHORT).show();
        } else {
            androidApplication = (AndroidApplication) mContext.getApplicationContext();
            User user = androidApplication.getUserInfo(mContext);
            updateUserProfileInteractor = new UpdateUserProfileInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, gender, name, mobile, stateId, cityId, pin, address);
            updateUserProfileInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingUserInfoSuccess(UserDetails user) {
        stateId = user.userProfile.stateId;
        cityId = user.userProfile.cityId;
        fetchCityList();
        mView.loadData(user);
        mView.hideLoader();
    }

    @Override
    public void onGettingUserInfoFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.goToLoginActivity();
        } else {
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onGettingCitiesSuccess(Cities[] cities) {
        if (cities.length == 0) {
            mView.hideCityRecyclerView();
        } else {
            cityListDialogAdapter = new CityListDialogAdapter(mContext, cities, this::onCityClicked);
            mView.loadCitiesAdapter(cityListDialogAdapter);
        }
    }

    @Override
    public void onGettingCitiesFail() {
        mView.hideCityRecyclerView();
    }

    @Override
    public void onGettingStateListSuccess(StateList[] states) {
        if (states.length == 0) {
            mView.hideStateRecyclerView();
        } else {
            stateListDialogAdapter = new StateListDialogAdapter(mContext, states, this::onStateClicked);
            mView.loadStateAdapter(stateListDialogAdapter);
        }
    }

    @Override
    public void onGettingStateListFail() {
        mView.hideStateRecyclerView();
    }

    @Override
    public void onCityClicked(int id, String city) {
        mView.setCityName(city);
        cityId = id;
    }

    @Override
    public void onStateClicked(int id, String state) {
        stateId = id;
        mView.setStateName(state);
        fetchCityList();
    }

    @Override
    public void onUserProfileUpdateSuccess() {
        Toast.makeText(mContext, "Profile Updated Successfully", Toast.LENGTH_SHORT).show();
        mView.hideLoader();
    }

    @Override
    public void onUserProfileUpdateFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.goToLoginActivity();
        } else {
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }
}
