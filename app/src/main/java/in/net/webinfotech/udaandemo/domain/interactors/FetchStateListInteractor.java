package in.net.webinfotech.udaandemo.domain.interactors;


import in.net.webinfotech.udaandemo.domain.model.StateList;

public interface FetchStateListInteractor {
    interface Callback {
        void onGettingStateListSuccess(StateList[] states);
        void onGettingStateListFail();
    }
}
