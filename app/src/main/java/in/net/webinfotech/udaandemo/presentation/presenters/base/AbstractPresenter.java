package in.net.webinfotech.udaandemo.presentation.presenters.base;


import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;


/**
 * Created by Raj on 05-01-2019.
 */

public abstract class AbstractPresenter {
    protected Executor mExecutor;
    protected MainThread mMainThread;
    public AbstractPresenter(Executor executor, MainThread mainThread){
        this.mExecutor = executor;
        this.mMainThread = mainThread;
    }
}
