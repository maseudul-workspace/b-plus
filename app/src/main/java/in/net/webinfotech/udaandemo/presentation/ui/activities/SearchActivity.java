package in.net.webinfotech.udaandemo.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.presentation.ui.dialogs.FilterByDialog;
import in.net.webinfotech.udaandemo.presentation.ui.dialogs.SortByBottomSheetDialog;

public class SearchActivity extends AppCompatActivity implements SortByBottomSheetDialog.Callback, FilterByDialog.Callback{

    androidx.appcompat.widget.SearchView searchView;
    SortByBottomSheetDialog sortByBottomSheetDialog;
    String selectedText = "Newest Arrivals";
    FilterByDialog filterByDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        initialiseFilterByDialog();
        filterByDialog.setUpDialogView();
    }

    public void initialiseFilterByDialog(){
        filterByDialog = new FilterByDialog(this, this, this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        searchView = (androidx.appcompat.widget.SearchView) myActionMenuItem.getActionView();
        myActionMenuItem.expandActionView();
        return true;
    }

    @OnClick(R.id.img_view_sort_by) void onSortByImageViewClicked(){
        Bundle args = new Bundle();
        args.putString("selectedText", selectedText);
        sortByBottomSheetDialog = new SortByBottomSheetDialog();
        sortByBottomSheetDialog.setArguments(args);
        sortByBottomSheetDialog.show(getSupportFragmentManager(), "Sort By");
    }

    @OnClick(R.id.img_view_filter_by) void onFilterByImageClicked(){
        filterByDialog.showDialog();
    }


    @Override
    public void onRadioBtnSelected(String msg) {

    }

    @Override
    public void onFiltersApplied(ArrayList<Integer> sellerIds, ArrayList<Integer> brandIds, ArrayList<Integer> colorIds, int minPrice, int maxPrice) {

    }
}
