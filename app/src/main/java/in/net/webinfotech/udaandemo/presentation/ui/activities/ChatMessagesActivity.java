package in.net.webinfotech.udaandemo.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.ChatMessagesAdapter;

public class ChatMessagesActivity extends AppCompatActivity {

    @BindView(R.id.reyclerview_message_list)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_messages);
        ButterKnife.bind(this);
        setMessageAdapter();
    }

    public void setMessageAdapter(){
        ChatMessagesAdapter chatMessagesAdapter = new ChatMessagesAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(chatMessagesAdapter);
    }

}
