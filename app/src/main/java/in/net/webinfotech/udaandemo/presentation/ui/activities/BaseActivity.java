package in.net.webinfotech.udaandemo.presentation.ui.activities;

import android.content.Intent;
import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.udaandemo.AndroidApplication;
import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.udaandemo.domain.model.User;
import in.net.webinfotech.udaandemo.presentation.presenters.BaseActivityPresenter;
import in.net.webinfotech.udaandemo.presentation.presenters.impl.BaseActivityPresenterImpl;
import in.net.webinfotech.udaandemo.threading.MainThreadImpl;

public class BaseActivity extends AppCompatActivity{

    @BindView(R.id.toolbar) @Nullable
    Toolbar toolbar;
    Drawer drawer;
    AndroidApplication androidApplication;
    @BindView(R.id.txt_cart_count)
    TextView txtViewCartCount;
    BaseActivityPresenterImpl mPresenter;
    @BindView(R.id.main_layout)
    ConstraintLayout constraintLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
//        initialisePresenter();
//        mPresenter.fetchCartCount();
    }


    protected void inflateContent(@LayoutRes int inflatedResID) {
        setContentView(R.layout.activity_base);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(inflatedResID, contentFrameLayout);
        ButterKnife.bind(this);
        setDrawer();

    }

    public void setDrawer(){
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
//        actionbar.setDisplayShowTitleEnabled(false);

// Create the AccountHeader
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.color.colorPrimaryDark)
                .addProfiles(new ProfileDrawerItem().withIcon(R.drawable.logo_blue).withName("B-Plus"))
                .build();

        drawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(headerResult)
                .withHasStableIds(true)
                .withDisplayBelowStatusBar(false)
                .withActionBarDrawerToggleAnimated(true)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName("Home").withIcon(R.drawable.home_outline).withSelectable(false).withIdentifier(1),
                        new PrimaryDrawerItem().withName("Address").withIcon(R.drawable.shipping_outline).withSelectable(false).withIdentifier(7),
                        new PrimaryDrawerItem().withName("Cart").withIcon(R.drawable.cart_outline).withSelectable(false).withIdentifier(4),
                        new PrimaryDrawerItem().withName("Orders").withIcon(R.drawable.orders_outline).withSelectable(false).withIdentifier(5),
                        new PrimaryDrawerItem().withName("Profile").withIcon(R.drawable.user_outline).withSelectable(false).withIdentifier(6)


                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if(drawerItem != null) {
                            switch (((int) drawerItem.getIdentifier())) {
                                case 2:
                                    Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
                                    startActivity(loginIntent);
                                    break;
                                case 7:
                                    if (isLoggedIn()) {
                                        Intent addressIntent = new Intent(getApplicationContext(), ShippingAddressActivity.class);
                                        startActivity(addressIntent);
                                    } else {
                                        showLogInSnackbar();
                                    }
                                    break;
                                case 3:
                                    androidApplication = (AndroidApplication) getApplicationContext();
                                    androidApplication.setUserInfo(getApplicationContext(), null);
                                    drawer.removeItem(3);
                                    drawer.addItem(new PrimaryDrawerItem().withName("Log In").withIcon(R.drawable.login_outline).withSelectable(false).withIdentifier(2));
                                    Toast.makeText(getApplicationContext(), "Successfully logged out", Toast.LENGTH_SHORT).show();
                                    break;
                                case 5:
                                    if (isLoggedIn()) {
                                        Intent orderHistoryIntent = new Intent(getApplicationContext(), OrderListActivity.class);
                                        startActivity(orderHistoryIntent);
                                    } else {
                                        showLogInSnackbar();
                                    }
                                    break;
                                case 6:
                                    if (isLoggedIn()) {
                                        Intent userProfileIntent = new Intent(getApplicationContext(), UserProfileActivity.class);
                                        startActivity(userProfileIntent);
                                    } else {
                                        showLogInSnackbar();
                                    }
                                    break;
                            }
                        }
                        return false;
                    }
                }).build();
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
        androidApplication = (AndroidApplication) getApplicationContext();

        if(androidApplication.getUserInfo(this) == null){
            drawer.addItem(new PrimaryDrawerItem().withName("Log In").withIcon(R.drawable.login_outline).withSelectable(false).withIdentifier(2));
        }else{
            Log.e("LogMsg", "API Key: " + androidApplication.getUserInfo(this).apiToken);
            Log.e("LogMsg", "User Id: " + androidApplication.getUserInfo(this).userId);
            drawer.addItem(new PrimaryDrawerItem().withName("Log Out").withIcon(R.drawable.logout_outline).withSelectable(false).withIdentifier(3));
        }

    }

    @OnClick(R.id.img_view_cart) void onCartClicked() {
        if (isLoggedIn()) {
            Intent intent = new Intent(this, CartActivity.class);
            startActivity(intent);
        } else {
            showLogInSnackbar();
        }
    }

    public void loadBaseCartCount(int count) {
        if (count == 0) {
            txtViewCartCount.setText("");
        } else {
            txtViewCartCount.setText(Integer.toString(count));
        }
    }

    public void showLogInSnackbar(){
        Toast.makeText(this, "Please log in ", Toast.LENGTH_LONG).show();

    }

    public boolean isLoggedIn() {
        androidApplication = (AndroidApplication) getApplicationContext();
        User user = androidApplication.getUserInfo(this);
        if (user == null) {
            return false;
        } else {
            return true;
        }
    }

}
