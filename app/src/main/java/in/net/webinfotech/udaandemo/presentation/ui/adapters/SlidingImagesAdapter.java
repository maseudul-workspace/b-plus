package in.net.webinfotech.udaandemo.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.model.MainSliders;
import in.net.webinfotech.udaandemo.util.GlideHelper;

/**
 * Created by Raj on 25-04-2019.
 */

public class SlidingImagesAdapter extends PagerAdapter {

    public interface Callback{
        void onImageClicked();
    }

    Context mContext;
    MainSliders[] mainSliders;
    Callback mCallback;
    int customPosition = 0;

    public SlidingImagesAdapter(Context mContext, MainSliders[] mainSliders, Callback callback) {
        this.mContext = mContext;
        this.mainSliders = mainSliders;
        this.mCallback = callback;
    }

    @Override
    public int getCount() {
        return mainSliders.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView imageView = new ImageView(mContext);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        GlideHelper.setImageView(mContext, imageView, mContext.getResources().getString(R.string.api_url) + "slider/image/" + mainSliders[position].slider);
        container.addView(imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onImageClicked();
            }
        });
        return imageView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
