package in.net.webinfotech.udaandemo.domain.interactors.impl;

import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.GetProductListWithFiltersInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.udaandemo.domain.model.ProductListWithFilters;
import in.net.webinfotech.udaandemo.domain.model.ProductListWithFiltersWrapper;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;

/**
 * Created by Raj on 02-09-2019.
 */

public class GetProductListWithFiltersInteractorImpl extends AbstractInteractor implements GetProductListWithFiltersInteractor {

    Callback mCallback;
    AppRepositoryImpl mRespository;
    int subcategoryId;
    int sellerId;
    int pageNo;

    public GetProductListWithFiltersInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRespository, int subcategoryId, int sellerId, int pageNo) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRespository = mRespository;
        this.subcategoryId = subcategoryId;
        this.sellerId = sellerId;
        this.pageNo = pageNo;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductListFail(errorMsg);
            }
        });
    }

    private void postMessage(final ProductListWithFilters productListWithFilters, int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductListSuccess(productListWithFilters, totalPage);
            }
        });
    }

    @Override
    public void run() {

        ProductListWithFiltersWrapper productListWithFiltersWrapper = mRespository.getProductListWithFilters(subcategoryId, sellerId, pageNo);

        if (productListWithFiltersWrapper == null) {
            notifyError("Something went wrong");
        } else if (!productListWithFiltersWrapper.status) {
            notifyError(productListWithFiltersWrapper.message);
        } else {
            postMessage(productListWithFiltersWrapper.productListWithFilters, productListWithFiltersWrapper.totalPage);
        }
    }
}
