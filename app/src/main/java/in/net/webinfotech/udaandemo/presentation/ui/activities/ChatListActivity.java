package in.net.webinfotech.udaandemo.presentation.ui.activities;

import android.content.Intent;
import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.ChatListAdapter;

public class ChatListActivity extends AppCompatActivity implements ChatListAdapter.Callback{

    @BindView(R.id.recycler_view_chat_list)
    RecyclerView recyclerViewChatList;
    @BindView(R.id.navigation)
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_list);
        ButterKnife.bind(this);
        setRecyclerViewChatList();
        setBottomNavigationView();
    }

    public void setRecyclerViewChatList(){
        ChatListAdapter adapter = new ChatListAdapter(this, this);
        recyclerViewChatList.setAdapter(adapter);
        recyclerViewChatList.setLayoutManager(new LinearLayoutManager(this));
    }

    public void setBottomNavigationView(){
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(1);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_home:
                        Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(mainIntent);
                        break;

                }
                return false;
            }
        });

    }


    @Override
    public void onSellerClicked() {
        Intent chatsIntent = new Intent(this, ChatMessagesActivity.class);
        startActivity(chatsIntent);
    }
}
