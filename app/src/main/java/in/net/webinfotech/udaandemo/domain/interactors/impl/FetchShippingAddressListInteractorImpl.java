package in.net.webinfotech.udaandemo.domain.interactors.impl;

import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.FetchShippingAddressListInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.udaandemo.domain.model.Address;
import in.net.webinfotech.udaandemo.domain.model.AddressListWrapper;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;

public class FetchShippingAddressListInteractorImpl extends AbstractInteractor implements FetchShippingAddressListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiToken;

    public FetchShippingAddressListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int userId, String apiToken) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiToken = apiToken;
    }

    private void notifyError(String errorMsg, int isLoginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingShippingAddressFail(errorMsg, isLoginError);
            }
        });
    }

    private void postMessage(Address[] addresses){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingShippingAddressSuccess(addresses);
            }
        });
    }

    @Override
    public void run() {
        final AddressListWrapper addressListWrapper = mRepository.fetchShippingAddressList(userId, apiToken);
        if (addressListWrapper == null) {
            notifyError("Something went wrong", 0);
        } else if (!addressListWrapper.status) {
            notifyError(addressListWrapper.message, addressListWrapper.login_error);
        } else {
            postMessage(addressListWrapper.addresses);
        }
    }
}
