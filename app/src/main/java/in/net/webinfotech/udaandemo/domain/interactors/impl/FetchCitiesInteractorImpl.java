package in.net.webinfotech.udaandemo.domain.interactors.impl;

import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.FetchCitiesInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.udaandemo.domain.model.Cities;
import in.net.webinfotech.udaandemo.domain.model.CitiesWrapper;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;

public class FetchCitiesInteractorImpl extends AbstractInteractor implements FetchCitiesInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int stateId;

    public FetchCitiesInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int stateId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.stateId = stateId;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCitiesFail();
            }
        });
    }

    private void postMessage(Cities[] cities){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCitiesSuccess(cities);
            }
        });
    }

    @Override
    public void run() {
        final CitiesWrapper citiesWrapper = mRepository.fetchCities(stateId);
        if (citiesWrapper == null) {
            notifyError();
        } else if (!citiesWrapper.status) {
            notifyError();
        } else {
            postMessage(citiesWrapper.cities);
        }
    }
}
