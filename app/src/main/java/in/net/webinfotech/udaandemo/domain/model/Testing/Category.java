package in.net.webinfotech.udaandemo.domain.model.Testing;

/**
 * Created by Raj on 25-04-2019.
 */

public class Category {
    public String categoryName;
    public String categoryImage;

    public Category(String categoryName, String categoryImage) {
        this.categoryName = categoryName;
        this.categoryImage = categoryImage;
    }
}
