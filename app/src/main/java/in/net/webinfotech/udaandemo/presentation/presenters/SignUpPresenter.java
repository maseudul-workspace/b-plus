package in.net.webinfotech.udaandemo.presentation.presenters;

public interface SignUpPresenter {
    void signUp(String name, String email, String mobile, String password, String confirmPassword);
    interface View {
        void showLoader();
        void hideLoader();
        void goToLoginActivity();
    }
}
