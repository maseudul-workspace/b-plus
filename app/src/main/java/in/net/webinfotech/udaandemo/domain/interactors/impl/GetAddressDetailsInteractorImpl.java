package in.net.webinfotech.udaandemo.domain.interactors.impl;


import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.GetAddressDetailsInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.udaandemo.domain.model.Address;
import in.net.webinfotech.udaandemo.domain.model.AddressDetailsWrapper;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;

public class GetAddressDetailsInteractorImpl extends AbstractInteractor implements GetAddressDetailsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int addressId;

    public GetAddressDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int addressId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.addressId = addressId;
    }

    private void notifyError(String errorMsg, int isLoginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingAddressDetailsFail(errorMsg, isLoginError);
            }
        });
    }

    private void postMessage(Address addresses){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingAddressDetailsSuccess(addresses);
            }
        });
    }

    @Override
    public void run() {
        AddressDetailsWrapper addressDetailsWrapper = mRepository.getShippingAddressDetails(apiToken, addressId);
        if (addressDetailsWrapper == null) {
            notifyError("Something went wrong", 0);
        } else if (!addressDetailsWrapper.status) {
            notifyError(addressDetailsWrapper.message, addressDetailsWrapper.login_error);
        } else {
            postMessage(addressDetailsWrapper.addresses);
        }
    }
}
