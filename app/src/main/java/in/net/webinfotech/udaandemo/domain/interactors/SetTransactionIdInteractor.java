package in.net.webinfotech.udaandemo.domain.interactors;

public interface SetTransactionIdInteractor {
    interface Callback {
        void onSetTransactionIdSuccess();
        void onSetTransactionIdFail(String errorMsg, int loginError);
    }
}
