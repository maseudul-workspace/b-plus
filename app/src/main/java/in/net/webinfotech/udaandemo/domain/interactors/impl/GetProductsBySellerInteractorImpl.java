package in.net.webinfotech.udaandemo.domain.interactors.impl;

import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.GetProductsBySellerInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.udaandemo.domain.model.SecondCategory;
import in.net.webinfotech.udaandemo.domain.model.SellerListWrapper;
import in.net.webinfotech.udaandemo.domain.model.SellersList;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;

/**
 * Created by Raj on 29-08-2019.
 */

public class GetProductsBySellerInteractorImpl extends AbstractInteractor implements GetProductsBySellerInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    int id;
    int pageNo;

    public GetProductsBySellerInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, int id, int pageNo) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.id = id;
        this.pageNo = pageNo;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductsListFail(errorMsg);
            }
        });
    }

    private void postMessage(final SellersList[] sellersLists, int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductsListSuccess(sellersLists, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final SellerListWrapper sellerListWrapper = mRepository.getProductsBySellers(id, pageNo);
        if (sellerListWrapper == null) {
            notifyError("Something went wrong");
        } else if (!sellerListWrapper.status) {
            notifyError(sellerListWrapper.message);
        } else {
            postMessage(sellerListWrapper.sellersLists, sellerListWrapper.totalPage);
        }
    }
}
