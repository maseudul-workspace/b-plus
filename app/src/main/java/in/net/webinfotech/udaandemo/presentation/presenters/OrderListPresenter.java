package in.net.webinfotech.udaandemo.presentation.presenters;

import in.net.webinfotech.udaandemo.presentation.ui.adapters.OrderListAdapter;

public interface OrderListPresenter {
    void fetchOrdersList();
    interface View {
        void loadAdapter(OrderListAdapter orderListAdapter);
        void showLoader();
        void hideLoader();
        void goToLoginActivity();
    }
}
