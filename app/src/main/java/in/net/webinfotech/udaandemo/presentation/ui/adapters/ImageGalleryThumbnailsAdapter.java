package in.net.webinfotech.udaandemo.presentation.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.model.Testing.Thumbnails;
import in.net.webinfotech.udaandemo.util.GlideHelper;

/**
 * Created by Raj on 07-05-2019.
 */

public class ImageGalleryThumbnailsAdapter extends RecyclerView.Adapter<ImageGalleryThumbnailsAdapter.ViewHolder>{

    public interface Callback{
        void onThumbnailClicked(int position);
    }

    Context mContext;
    ArrayList<Thumbnails> thumbnailsArrayList;
    Callback mCallback;

    public ImageGalleryThumbnailsAdapter(Context mContext, ArrayList<Thumbnails> thumbnailsArrayList, Callback callback) {
        this.mContext = mContext;
        this.thumbnailsArrayList = thumbnailsArrayList;
        this.mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_image_gallery_thumbnails, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        GlideHelper.setImageView(mContext, holder.imgViewThumbnail, thumbnailsArrayList.get(position).imageUrl);
        if(thumbnailsArrayList.get(position).isSelected){
            holder.relativeLayoutThumbnail.setBackgroundColor(mContext.getResources().getColor(R.color.black54));
        }else{
            holder.relativeLayoutThumbnail.setBackgroundColor(Color.TRANSPARENT);
        }
        holder.imgViewThumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onThumbnailClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return thumbnailsArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.recycler_view_relative_layout_image_thumbnail)
        RelativeLayout relativeLayoutThumbnail;
        @BindView(R.id.img_view_thumbnail)
        ImageView imgViewThumbnail;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateThumbnils(ArrayList<Thumbnails> thumbnailsArrayList){
        this.thumbnailsArrayList = thumbnailsArrayList;
        notifyDataSetChanged();
    }

}
