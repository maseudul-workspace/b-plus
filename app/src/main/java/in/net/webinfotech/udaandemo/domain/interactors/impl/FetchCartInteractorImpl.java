package in.net.webinfotech.udaandemo.domain.interactors.impl;

import android.content.Context;

import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.FetchCartsInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.udaandemo.domain.model.Cart;
import in.net.webinfotech.udaandemo.domain.model.CartsWrapper;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;

public class FetchCartInteractorImpl extends AbstractInteractor implements FetchCartsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;

    public FetchCartInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCartDetailsFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(Cart[] carts){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCartDetailsSuccess(carts);
            }
        });
    }

    @Override
    public void run() {
        final CartsWrapper cartsWrapper = mRepository.fetchCartDetails(userId, apiToken);
        if (cartsWrapper == null) {
            notifyError("Something went wrong", cartsWrapper.loginError);
        } else if(!cartsWrapper.status) {
            notifyError(cartsWrapper.message, cartsWrapper.loginError);
        } else {
            postMessage(cartsWrapper.carts);
        }
    }
}
