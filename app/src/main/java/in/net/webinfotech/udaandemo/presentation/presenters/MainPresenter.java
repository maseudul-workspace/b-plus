package in.net.webinfotech.udaandemo.presentation.presenters;

import in.net.webinfotech.udaandemo.domain.model.MainData;

/**
 * Created by Raj on 28-08-2019.
 */

public interface MainPresenter {
    void fetchHomeData();
    void fetchCartItems();
    interface View {
        void loadMainData(MainData mainData);
        void loadCartCount(int count);
    }
}
