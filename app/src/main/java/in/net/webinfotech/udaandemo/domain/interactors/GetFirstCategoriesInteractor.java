package in.net.webinfotech.udaandemo.domain.interactors;

import in.net.webinfotech.udaandemo.domain.model.FirstCategory;

/**
 * Created by Raj on 28-08-2019.
 */

public interface GetFirstCategoriesInteractor {
    interface Callback {
        void onGettingFirstCategoriesSuccess(FirstCategory[] firstCategories);
        void onGettingFirstCategoriesFail(String erroMsg);
    }
}
