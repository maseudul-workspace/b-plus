package in.net.webinfotech.udaandemo.domain.interactors;

import in.net.webinfotech.udaandemo.domain.model.SecondCategory;

/**
 * Created by Raj on 29-08-2019.
 */

public interface GetSecondCategoryInteractor {
    interface Callback {
        void onGettingSecondCategoriesSuccess(SecondCategory[] secondCategories);
        void onGettingSecondCategoriesFail(String errorMsg);
    }
}
