package in.net.webinfotech.udaandemo.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 02-09-2019.
 */

public class ProductListWithFilters {

    @SerializedName("filter_data")
    @Expose
    public FilterData filterData;

    @SerializedName("products")
    @Expose
    public Product[] products;

}
