package in.net.webinfotech.udaandemo.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 30-08-2019.
 */

public class Brands {
    @SerializedName("brand_id")
    @Expose
    public int brandId;

    @SerializedName("brand_name")
    @Expose
    public String brandName;

    @SerializedName("total")
    @Expose
    public int total;
}
