package in.net.webinfotech.udaandemo.domain.interactors;

import in.net.webinfotech.udaandemo.domain.model.MainData;

/**
 * Created by Raj on 28-08-2019.
 */

public interface GetMainDataInteractor {
    interface Callback {
        void onGettingMainDataSuccess(MainData mainData);
        void onGettingMainDataFail();
    }
}
