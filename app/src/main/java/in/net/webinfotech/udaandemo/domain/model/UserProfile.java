package in.net.webinfotech.udaandemo.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserProfile {
    @SerializedName("pin")
    @Expose
    public String pin;

    @SerializedName("s_name")
    @Expose
    public String stateName;

    @SerializedName("c_name")
    @Expose
    public String cityName;

    @SerializedName("address")
    @Expose
    public String address;

    @SerializedName("gender")
    @Expose
    public String gender;

    @SerializedName("state_id")
    @Expose
    public int stateId;

    @SerializedName("city_id")
    @Expose
    public int cityId;
}
