package in.net.webinfotech.udaandemo.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.SignUpInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.impl.SignUpInteractorImpl;
import in.net.webinfotech.udaandemo.presentation.presenters.SignUpPresenter;
import in.net.webinfotech.udaandemo.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;

public class SignUpPresenterImpl extends AbstractPresenter implements SignUpPresenter, SignUpInteractor.Callback {

    Context mContext;
    SignUpPresenter.View mView;
    SignUpInteractorImpl signUpInteractor;

    public SignUpPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void signUp(String name, String email, String mobile, String password, String confirmPassword) {
        signUpInteractor = new SignUpInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), name, email, password, confirmPassword, mobile);
        signUpInteractor.execute();
    }

    @Override
    public void onSignUpSuccess() {
        mView.hideLoader();
        Toast.makeText(mContext, "Registered Successfully", Toast.LENGTH_SHORT).show();
        mView.goToLoginActivity();
    }

    @Override
    public void onSignUpFailed(String errorMsg) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
