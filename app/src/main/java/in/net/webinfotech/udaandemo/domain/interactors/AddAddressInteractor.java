package in.net.webinfotech.udaandemo.domain.interactors;

public interface AddAddressInteractor {
    interface Callback {
        void onAddAddressSuccess();
        void onAddAddressFail(String errorMsg, int loginError);
    }
}
