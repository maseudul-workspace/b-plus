package in.net.webinfotech.udaandemo.domain.interactors.impl;

import android.util.Log;

import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.GetAccessTokenInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.udaandemo.domain.model.AccessTokenResponse;
import in.net.webinfotech.udaandemo.repository.PaymentRepository;
import in.net.webinfotech.udaandemo.repository.PaymentRepositoryImpl;

public class GetAccessTokenInteractorImpl extends AbstractInteractor implements GetAccessTokenInteractor {

    PaymentRepositoryImpl mRepository;
    Callback mCallback;
    String clientId;
    String clientSecret;
    String grantType;

    public GetAccessTokenInteractorImpl(Executor threadExecutor, MainThread mainThread, PaymentRepositoryImpl mRepository, Callback mCallback, String clientId, String clientSecret, String grantType) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.grantType = grantType;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingAccessTokenFail(errorMsg);
            }
        });
    }

    private void postMessage(String accessToken){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingAccessTokenSuccess(accessToken);
            }
        });
    }

    @Override
    public void run() {
        AccessTokenResponse accessTokenResponse = mRepository.fetchAccessToken(clientId, clientSecret, grantType);
        if (accessTokenResponse == null) {
            notifyError("Something went wrong");
        } else if (!accessTokenResponse.error.isEmpty()) {
            notifyError(accessTokenResponse.error);
        } else {
            postMessage(accessTokenResponse.accessToken);
        }
    }
}
