package in.net.webinfotech.udaandemo.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.udaandemo.domain.model.UserProfile;
import in.net.webinfotech.udaandemo.presentation.presenters.OrderListPresenter;
import in.net.webinfotech.udaandemo.presentation.presenters.impl.OrderListPresenterImpl;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.OrderListAdapter;
import in.net.webinfotech.udaandemo.threading.MainThreadImpl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class OrderListActivity extends AppCompatActivity implements OrderListPresenter.View {

    @BindView(R.id.recycler_view_order_list)
    RecyclerView recyclerView;
    ProgressDialog progressDialog;
    OrderListPresenterImpl mPresenter;
    @BindView(R.id.navigation)
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        ButterKnife.bind(this);
        setBottomNavigationView();
        getSupportActionBar().setTitle("Order History");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setProgressDialog();
        initialisePresenter();
        mPresenter.fetchOrdersList();
        showLoader();
    }

    public void setBottomNavigationView(){
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(1);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_home:
                        Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(mainIntent);
                        break;
                    case R.id.action_profile:
                        Intent intent1 = new Intent(getApplicationContext(), UserProfileActivity.class);
                        startActivity(intent1);
                        break;
                }
                return false;
            }
        });

    }

    public void initialisePresenter() {
        mPresenter = new OrderListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadAdapter(OrderListAdapter orderListAdapter) {
        recyclerView.setAdapter(orderListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void goToLoginActivity() {
        Toast.makeText(this, "Your session expired !! Please login again", Toast.LENGTH_LONG).show();
        Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(loginIntent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
