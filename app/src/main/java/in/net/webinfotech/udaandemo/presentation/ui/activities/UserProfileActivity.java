package in.net.webinfotech.udaandemo.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.udaandemo.domain.model.User;
import in.net.webinfotech.udaandemo.domain.model.UserDetails;
import in.net.webinfotech.udaandemo.domain.model.UserProfile;
import in.net.webinfotech.udaandemo.presentation.presenters.UserProfilePresenter;
import in.net.webinfotech.udaandemo.presentation.presenters.impl.AddAddressPresenterImpl;
import in.net.webinfotech.udaandemo.presentation.presenters.impl.UserProfilePresenterImpl;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.CityListDialogAdapter;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.StateListDialogAdapter;
import in.net.webinfotech.udaandemo.presentation.ui.dialogs.SelectCityDialog;
import in.net.webinfotech.udaandemo.presentation.ui.dialogs.SelectStateDialog;
import in.net.webinfotech.udaandemo.threading.MainThreadImpl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.textfield.TextInputLayout;

public class UserProfileActivity extends AppCompatActivity implements UserProfilePresenter.View {

    SelectStateDialog selectStateDialog;
    @BindView(R.id.txt_view_state)
    TextView txtViewState;
    SelectCityDialog selectCityDialog;
    @BindView(R.id.txt_view_city_name)
    TextView txtViewCityName;
    @BindView(R.id.txt_input_email_layout)
    TextInputLayout txtInputEmailLayout;
    @BindView(R.id.txt_input_phone_layout)
    TextInputLayout txtInputPhoneLayout;
    @BindView(R.id.txt_input_address_layout)
    TextInputLayout txtInputAddressLayout;
    @BindView(R.id.txt_input_pincode_layout)
    TextInputLayout txtInputPincodeLayout;
    @BindView(R.id.txt_input_name_layout)
    TextInputLayout txtInputNameLayout;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_pincode)
    EditText editTextPincode;
    @BindView(R.id.edit_text_address)
    EditText editTextAddress;
    @BindView(R.id.radio_group_gender)
    RadioGroup radioGroupGender;
    @BindView(R.id.radio_btn_male)
    RadioButton radioButtonMale;
    @BindView(R.id.radio_btn_female)
    RadioButton radioButtonFemale;
    ProgressDialog progressDialog;
    UserProfilePresenterImpl mPresenter;
    @BindView(R.id.navigation)
    BottomNavigationView bottomNavigationView;
    String gender = "M";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        ButterKnife.bind(this);
        setRadioGroupGender();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("User Profile");
        setBottomNavigationView();
        setProgressDialog();
        initialisePresenter();
        setUpStateSelectDialogView();
        setSelectCityDialog();
        mPresenter.fetchStateList();
        mPresenter.fetchUserProfile();
        showLoader();
    }

    public void setRadioGroupGender() {
        radioGroupGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb=(RadioButton)findViewById(checkedId);
                if (rb.getText().toString().equals("Male")) {
                    gender = "M";
                } else {
                    gender = "F";
                }
            }
        });
    }

    public void setBottomNavigationView(){
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(2);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_home:
                        Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(mainIntent);
                        break;
                    case R.id.action_order:
                        Intent intent1 = new Intent(getApplicationContext(), OrderListActivity.class);
                        startActivity(intent1);
                        break;
                }
                return false;
            }
        });

    }

    public void initialisePresenter() {
        mPresenter = new UserProfilePresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    public void setUpStateSelectDialogView() {
        selectStateDialog = new SelectStateDialog(this, this);
        selectStateDialog.setUpDialogView();
    }

    public void setSelectCityDialog() {
        selectCityDialog = new SelectCityDialog(this, this);
        selectCityDialog.setUpDialogView();
    }


    @Override
    public void loadData(UserDetails userDetails) {
        User user = userDetails.user;
        UserProfile userProfile = userDetails.userProfile;
        if (user.name != null) {
            editTextName.setText(user.name);
        }
        if (user.email != null) {
            editTextEmail.setText(user.email);
        }
        if (user.mobile != null) {
            editTextPhone.setText(user.mobile);
        }
        if (userProfile.stateName != null) {
            txtViewState.setText(userProfile.stateName);
            txtViewState.setTextColor(this.getResources().getColor(R.color.black1));
        }
        if (userProfile.cityName != null) {
            txtViewCityName.setText(userProfile.cityName);
            txtViewCityName.setTextColor(this.getResources().getColor(R.color.black1));
        }
        if (userProfile.pin != null) {
            editTextPincode.setText(userProfile.pin);
        }
        if (userProfile.address != null) {
            editTextAddress.setText(userProfile.address);
        }

        if (userProfile.gender != null) {
            if (userProfile.gender.equals("M")) {
                gender = "M";
                radioButtonMale.setChecked(true);
                radioButtonFemale.setChecked(false);
            } else {
                gender = "F";
                radioButtonFemale.setChecked(true);
                radioButtonMale.setChecked(false);
            }
        }
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void hideStateRecyclerView() {
        selectCityDialog.hideRecyclerView();
    }

    @Override
    public void hideCityRecyclerView() {
        selectCityDialog.hideRecyclerView();
    }

    @Override
    public void loadStateAdapter(StateListDialogAdapter stateListDialogAdapter) {
        selectStateDialog.setRecyclerView(stateListDialogAdapter);
    }

    @Override
    public void loadCitiesAdapter(CityListDialogAdapter cityListDialogAdapter) {
        selectCityDialog.setRecyclerView(cityListDialogAdapter);
    }

    @Override
    public void setStateName(String stateName) {
        txtViewState.setText(stateName);
        txtViewState.setTextColor(this.getResources().getColor(R.color.black1));
        selectStateDialog.hideDialog();
    }

    @Override
    public void setCityName(String cityName) {
        txtViewCityName.setText(cityName);
        txtViewCityName.setTextColor(this.getResources().getColor(R.color.black1));
        selectCityDialog.hideDialog();
    }

    @Override
    public void goToLoginActivity() {
        Toast.makeText(this, "Your session expired !! Please login again", Toast.LENGTH_LONG).show();
        Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(loginIntent);
    }

    @OnClick(R.id.state_linear_layout) void onStateClicked() {
        selectStateDialog.showDialog();
    }

    @OnClick(R.id.city_linear_layout) void onCityClicked() {
        selectCityDialog.showDialog();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.btn_update) void onUpdateClicked() {
        if (editTextPhone.getText().toString().trim().isEmpty()) {
            txtInputPhoneLayout.setError("Phone Required");
            editTextPhone.requestFocus();
        } else if (editTextPincode.getText().toString().trim().isEmpty()) {
            txtInputPincodeLayout.setError("Pincode Required");
            editTextPincode.requestFocus();
        } else if (editTextAddress.getText().toString().trim().isEmpty()) {
            txtInputAddressLayout.setError("Address Required");
            editTextAddress.requestFocus();
        } else if (editTextName.getText().toString().trim().isEmpty()) {
            txtInputNameLayout.setError("Name Required");
            editTextName.requestFocus();
        } else {
            mPresenter.updateUserProfile(gender, editTextName.getText().toString(), editTextPhone.getText().toString(), editTextPincode.getText().toString(), editTextAddress.getText().toString());
            showLoader();
        }
    }

}
