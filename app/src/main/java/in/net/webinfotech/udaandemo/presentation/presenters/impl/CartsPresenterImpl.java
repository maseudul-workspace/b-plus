package in.net.webinfotech.udaandemo.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.udaandemo.AndroidApplication;
import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.DeleteItemFromCartInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.FetchCartsInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.UpdateCartInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.impl.DeleteItemFromCartInteractorImpl;
import in.net.webinfotech.udaandemo.domain.interactors.impl.FetchCartInteractorImpl;
import in.net.webinfotech.udaandemo.domain.interactors.impl.UpdateCartInteractorImpl;
import in.net.webinfotech.udaandemo.domain.model.Cart;
import in.net.webinfotech.udaandemo.domain.model.User;
import in.net.webinfotech.udaandemo.presentation.presenters.CartsPresenter;
import in.net.webinfotech.udaandemo.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.CartAdapter;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;

public class CartsPresenterImpl extends AbstractPresenter implements CartsPresenter, FetchCartsInteractor.Callback, CartAdapter.Callback, DeleteItemFromCartInteractor.Callback, UpdateCartInteractor.Callback {

    Context mContext;
    CartsPresenter.View mView;
    FetchCartInteractorImpl fetchCartInteractor;
    AndroidApplication androidApplication;
    DeleteItemFromCartInteractorImpl deleteItemFromCartInteractor;
    UpdateCartInteractorImpl updateCartInteractor;

    public CartsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchCartProducts() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        fetchCartInteractor = new FetchCartInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId);
        fetchCartInteractor.execute();
    }

    @Override
    public void onGettingCartDetailsSuccess(Cart[] carts) {
        if (carts.length > 0) {
            double cartTotal = 0;
            for (int i = 0; i < carts.length; i++) {
                cartTotal = cartTotal + carts[i].productMRP*carts[i].quantity;
            }
            CartAdapter cartAdapter = new CartAdapter(mContext, carts, this);
            mView.loadData(cartAdapter, cartTotal);
        } else {
            mView.hideCartRecyclerView();
        }
        mView.hideLoader();

    }

    @Override
    public void onGettingCartDetailsFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.goToLoginActivity();
        } else {
            mView.hideCartRecyclerView();
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onDeleteClicked(int cartId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        deleteItemFromCartInteractor = new DeleteItemFromCartInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, cartId);
        deleteItemFromCartInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onUpdateClicked(int cartId, int qty) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        updateCartInteractor = new UpdateCartInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, qty, cartId);
        updateCartInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onCartItemDeleteSuccess() {
        Toast.makeText(mContext, "Deleted Successfully", Toast.LENGTH_SHORT).show();
        fetchCartProducts();
        mView.hideCartRecyclerView();
    }

    @Override
    public void onCartItemDeleteFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.goToLoginActivity();
        } else {
            Toast.makeText(mContext, "Unable to delete", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onUpdateCartSuccess() {
        fetchCartProducts();
        Toast.makeText(mContext, "Successfully updated", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpdateCartFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.goToLoginActivity();
        } else {
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }
}
