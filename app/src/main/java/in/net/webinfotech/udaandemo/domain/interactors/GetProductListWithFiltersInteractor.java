package in.net.webinfotech.udaandemo.domain.interactors;

import in.net.webinfotech.udaandemo.domain.model.ProductListWithFilters;

/**
 * Created by Raj on 02-09-2019.
 */

public interface GetProductListWithFiltersInteractor {
    interface Callback {
        void onGettingProductListSuccess(ProductListWithFilters productListWithFilters, int totalPage);
        void onGettingProductListFail(String errorMsg);
    }
}
