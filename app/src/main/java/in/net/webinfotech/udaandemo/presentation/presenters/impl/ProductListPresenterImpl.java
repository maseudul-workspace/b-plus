package in.net.webinfotech.udaandemo.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.GetProductListWithFiltersInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.GetProductListWithoutFilterInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.impl.GetProductListWithFiltersInteractorImpl;
import in.net.webinfotech.udaandemo.domain.interactors.impl.GetProductListWithoutFilterInteractorImpl;
import in.net.webinfotech.udaandemo.domain.model.Product;
import in.net.webinfotech.udaandemo.domain.model.ProductListWithFilters;
import in.net.webinfotech.udaandemo.presentation.presenters.ProductListPresenter;
import in.net.webinfotech.udaandemo.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.ProductListHorizontalAdapter;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.ProductListVerticalAdapter;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;

/**
 * Created by Raj on 02-09-2019.
 */

public class ProductListPresenterImpl extends AbstractPresenter implements ProductListPresenter, GetProductListWithFiltersInteractor.Callback, ProductListVerticalAdapter.Callback, GetProductListWithoutFilterInteractor.Callback {

    Context mContext;
    ProductListPresenter.View mView;
    GetProductListWithFiltersInteractorImpl getProductListWithFiltersInteractor;
    ProductListVerticalAdapter adapter;
    GetProductListWithoutFilterInteractorImpl getProductListWithoutFilterInteractor;
    Product[] newProducts;

    public ProductListPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchProductListWithFilters(int subcategoryId, int sellerId, int pageNo, String type) {
        if (type.equals("refresh")) {
            newProducts = null;
        }
        getProductListWithFiltersInteractor = new GetProductListWithFiltersInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), subcategoryId, sellerId, pageNo);
        getProductListWithFiltersInteractor.execute();
    }

    @Override
    public void fetchProductListWithoutFilters(int secondCategory, ArrayList<Integer> sellerIds, ArrayList<Integer> brandIds, ArrayList<Integer> colorIds, int minPrice, int maxPrice, String sortBy, int page, String type) {
        if (type.equals("refresh")) {
            newProducts = null;
        }
        getProductListWithoutFilterInteractor = new GetProductListWithoutFilterInteractorImpl(
                mExecutor,
                mMainThread,
                new AppRepositoryImpl(),
                this,
                secondCategory,
                sellerIds,
                brandIds,
                colorIds,
                minPrice,
                maxPrice,
                sortBy,
                page
        );
        getProductListWithoutFilterInteractor.execute();
    }



    @Override
    public void onGettingProductListSuccess(ProductListWithFilters productListWithFilters, int totalPage) {
        mView.loadFilters(productListWithFilters.filterData);
        if(productListWithFilters.products.length > 0){
            Product[] tempProductLists;
            tempProductLists = newProducts;
            try {
                int len1 = tempProductLists.length;
                int len2 = productListWithFilters.products.length;
                newProducts = new Product[len1 + len2];
                System.arraycopy(tempProductLists, 0, newProducts, 0, len1);
                System.arraycopy(productListWithFilters.products, 0, newProducts, len1, len2);
                adapter.updateDataSet(newProducts);
                adapter.notifyDataSetChanged();
                mView.hidePaginationProgressBar();
            }catch (NullPointerException e){
                newProducts = productListWithFilters.products;
                adapter = new ProductListVerticalAdapter(mContext, productListWithFilters.products, this);
                mView.loadAdapter(adapter, totalPage);
            }
        }
        mView.hideLoader();

    }

    @Override
    public void onGettingProductListFail(String errorMsg) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        mView.hidePaginationProgressBar();
    }

    @Override
    public void onProductClicked(int productId) {
        mView.goToProductDetails(productId);
    }

    @Override
    public void onGettingProductListWithoutFilterSuccess(Product[] products, int totalPage) {
        adapter = new ProductListVerticalAdapter(mContext, products, this);
        if(products.length > 0){
            Product[] tempProductLists;
            tempProductLists = newProducts;
            try {
                int len1 = tempProductLists.length;
                int len2 = products.length;
                newProducts = new Product[len1 + len2];
                System.arraycopy(tempProductLists, 0, newProducts, 0, len1);
                System.arraycopy(products, 0, newProducts, len1, len2);
                adapter.updateDataSet(newProducts);
                adapter.notifyDataSetChanged();
                mView.hidePaginationProgressBar();
            }catch (NullPointerException e){
                newProducts = products;
                adapter = new ProductListVerticalAdapter(mContext, products, this);
                mView.loadAdapter(adapter, totalPage);
            }
        }
        mView.hideLoader();
    }

    @Override
    public void onGettingProductListWihoutFilterFail(String errorMsg) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        mView.hidePaginationProgressBar();
    }
}
