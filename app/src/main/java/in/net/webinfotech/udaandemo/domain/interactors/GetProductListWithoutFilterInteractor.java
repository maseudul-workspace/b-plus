package in.net.webinfotech.udaandemo.domain.interactors;

import in.net.webinfotech.udaandemo.domain.model.Product;

public interface GetProductListWithoutFilterInteractor {
    interface Callback {
        void onGettingProductListWithoutFilterSuccess(Product[] products, int totalPage);
        void onGettingProductListWihoutFilterFail(String errorMsg);
    }
}
