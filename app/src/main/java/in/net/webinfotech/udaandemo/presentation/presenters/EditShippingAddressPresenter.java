package in.net.webinfotech.udaandemo.presentation.presenters;

import in.net.webinfotech.udaandemo.domain.model.Address;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.CityListDialogAdapter;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.StateListDialogAdapter;

public interface EditShippingAddressPresenter {
    void getShippingAddressDetails(int addressId);
    void updateAddress(String email,
                       String mobile,
                       String pin,
                       String address);
    void fetchStateList();
    void fetchCityList();
    interface View {
        void showLoader();
        void hideLoader();
        void setData(Address address);
        void loadStateAdapter(StateListDialogAdapter stateListDialogAdapter);
        void loadCitiesAdapter(CityListDialogAdapter cityListDialogAdapter);
        void setStateName(String stateName);
        void setCityName(String cityName);
        void hideStateRecyclerView();
        void hideCityRecyclerView();
        void onUpdateAddressSuccess();
        void goToLoginActivity();
    }
}
