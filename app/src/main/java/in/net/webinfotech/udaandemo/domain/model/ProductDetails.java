package in.net.webinfotech.udaandemo.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 03-09-2019.
 */

public class ProductDetails {

    @SerializedName("product")
    @Expose
    public Product product;

    @SerializedName("product_images")
    @Expose
    public ProductImages[] productImages;

    @SerializedName("product_color")
    @Expose
    public Colors[] productColors;

    @SerializedName("seller_details")
    @Expose
    public Sellers sellers;

    @SerializedName("related_products")
    @Expose
    public Product[] relatedProducts;

}
