package in.net.webinfotech.udaandemo.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Colors {
/**
 * Created by Raj on 30-08-2019.
 */

    @SerializedName("color_id")
    @Expose
    public int colorId;

    @SerializedName("color_name")
    @Expose
    public String colorName;

    @SerializedName("color_value")
    @Expose
    public String colorValue;

    public boolean isSelected = false;
}
