package in.net.webinfotech.udaandemo.domain.interactors;

public interface UpdateCartInteractor {
    interface Callback {
        void onUpdateCartSuccess();
        void onUpdateCartFail(String errorMsg, int loginError);
    }
}
