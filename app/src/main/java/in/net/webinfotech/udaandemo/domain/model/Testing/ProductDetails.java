package in.net.webinfotech.udaandemo.domain.model.Testing;

/**
 * Created by Raj on 30-04-2019.
 */

public class ProductDetails {
    public String productName;
    public String image;
    public String seller;
    public double price;

    public ProductDetails(String productName, String image, String seller, double price) {
        this.productName = productName;
        this.image = image;
        this.seller = seller;
        this.price = price;
    }
}
