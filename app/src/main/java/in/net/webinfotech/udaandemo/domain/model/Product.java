package in.net.webinfotech.udaandemo.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 28-08-2019.
 */

public class Product {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("tag_name")
    @Expose
    public String tagNmae;

    @SerializedName("brand_id")
    @Expose
    public int brandId;


    @SerializedName("seller_id")
    @Expose
    public int sellerId;

    @SerializedName("category")
    @Expose
    public int category;

    @SerializedName("first_category")
    @Expose
    public int firstCategory;

    @SerializedName("second_category")
    @Expose
    public int secondCategory;

    @SerializedName("main_image")
    @Expose
    public String mainImage;

    @SerializedName("short_description")
    @Expose
    public String shortDescription;

    @SerializedName("long_description")
    @Expose
    public String longDescription;

    @SerializedName("mrp")
    @Expose
    public String mrp;

    @SerializedName("price")
    @Expose
    public String price;

    @SerializedName("min_ord_qtty")
    @Expose
    public int minimumOrderQty;

    @SerializedName("brand_name")
    @Expose
    public String brandName;

}
