package in.net.webinfotech.udaandemo.presentation.presenters.impl;

import android.content.Context;

import in.net.webinfotech.udaandemo.AndroidApplication;
import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.FetchCartsInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.impl.FetchCartInteractorImpl;
import in.net.webinfotech.udaandemo.domain.model.Cart;
import in.net.webinfotech.udaandemo.domain.model.User;
import in.net.webinfotech.udaandemo.presentation.presenters.BaseActivityPresenter;
import in.net.webinfotech.udaandemo.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;

public class BaseActivityPresenterImpl extends AbstractPresenter implements BaseActivityPresenter, FetchCartsInteractor.Callback {

    Context mContext;
    BaseActivityPresenter.View mView;
    FetchCartInteractorImpl fetchCartInteractor;
    AndroidApplication androidApplication;

    public BaseActivityPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchCartCount() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        if (user != null) {
            fetchCartInteractor = new FetchCartInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId);
            fetchCartInteractor.execute();
        }
    }

    @Override
    public void onGettingCartDetailsSuccess(Cart[] carts) {
        mView.loadCartCount(carts.length);
    }

    @Override
    public void onGettingCartDetailsFail(String errorMsg, int loginError) {
        mView.loadCartCount(0);
    }
}
