package in.net.webinfotech.udaandemo.repository;

import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;

import in.net.webinfotech.udaandemo.domain.model.AddressDetailsWrapper;
import in.net.webinfotech.udaandemo.domain.model.AddressListWrapper;
import in.net.webinfotech.udaandemo.domain.model.CartsWrapper;
import in.net.webinfotech.udaandemo.domain.model.CitiesWrapper;
import in.net.webinfotech.udaandemo.domain.model.CommonResponse;
import in.net.webinfotech.udaandemo.domain.model.FirstCategoryWrapper;
import in.net.webinfotech.udaandemo.domain.model.MainDataWrapper;
import in.net.webinfotech.udaandemo.domain.model.OrderListWrapper;
import in.net.webinfotech.udaandemo.domain.model.OrderPlaceDataResponse;
import in.net.webinfotech.udaandemo.domain.model.ProductDetailsWrapper;
import in.net.webinfotech.udaandemo.domain.model.ProductListWithFilters;
import in.net.webinfotech.udaandemo.domain.model.ProductListWithFiltersWrapper;
import in.net.webinfotech.udaandemo.domain.model.ProductListWrapper;
import in.net.webinfotech.udaandemo.domain.model.SecondCategoryWrapper;
import in.net.webinfotech.udaandemo.domain.model.SellerListWrapper;
import in.net.webinfotech.udaandemo.domain.model.StateListWrapper;
import in.net.webinfotech.udaandemo.domain.model.UserDetails;
import in.net.webinfotech.udaandemo.domain.model.UserDetailsWrapper;
import in.net.webinfotech.udaandemo.domain.model.UserWrapper;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.Header;

/**
 * Created by Raj on 28-08-2019.
 */

public class AppRepositoryImpl {

    AppRepository mRepository;

    public AppRepositoryImpl() {
        mRepository = APIclient.createService(AppRepository.class);
    }

    public MainDataWrapper fetchMainData() {
        MainDataWrapper mainDataWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchHomeData();

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    mainDataWrapper = null;
                }else{
                    mainDataWrapper = gson.fromJson(responseBody, MainDataWrapper.class);
                }
            } else {
                mainDataWrapper = null;
            }
        }catch (Exception e){
            mainDataWrapper = null;
        }
        return mainDataWrapper;
    }

    public FirstCategoryWrapper fetchFirstCategories(int id) {
        FirstCategoryWrapper firstCategoryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.getSubCategories(id);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    firstCategoryWrapper = null;
                }else{
                    firstCategoryWrapper = gson.fromJson(responseBody, FirstCategoryWrapper.class);
                }
            } else {
                firstCategoryWrapper = null;
            }
        }catch (Exception e){
            firstCategoryWrapper = null;
        }
        return firstCategoryWrapper;
    }

    public SecondCategoryWrapper fetchSecondCategory(int id) {
        SecondCategoryWrapper secondCategoryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.getSecondCategories(id);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    secondCategoryWrapper = null;
                }else{
                    secondCategoryWrapper = gson.fromJson(responseBody, SecondCategoryWrapper.class);
                }
            } else {
                secondCategoryWrapper = null;
            }
        }catch (Exception e){
            secondCategoryWrapper = null;
        }
        return secondCategoryWrapper;
    }

    public SellerListWrapper getProductsBySellers(int id, int pageNo) {
        SellerListWrapper sellerListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.getProductsBySellers(id, pageNo);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    sellerListWrapper = null;
                }else{
                    sellerListWrapper = gson.fromJson(responseBody, SellerListWrapper.class);
                }
            } else {
                sellerListWrapper = null;
            }
        }catch (Exception e){
            sellerListWrapper = null;
        }
        return sellerListWrapper;
    }

    public ProductListWithFiltersWrapper getProductListWithFilters(int id, int sellerId, int pageNo) {
        ProductListWithFiltersWrapper productListWithFiltersWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.getProductsListWithFilters(id, sellerId, pageNo);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    productListWithFiltersWrapper = null;
                }else{
                    productListWithFiltersWrapper = gson.fromJson(responseBody, ProductListWithFiltersWrapper.class);
                }
            } else {
                productListWithFiltersWrapper = null;
            }
        }catch (Exception e){
            productListWithFiltersWrapper = null;
        }
        return productListWithFiltersWrapper;
    }

    public ProductDetailsWrapper getProductDetails(int productId) {
        ProductDetailsWrapper productDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.getProductDetails(productId);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    productDetailsWrapper = null;
                }else{
                    productDetailsWrapper = gson.fromJson(responseBody, ProductDetailsWrapper.class);
                }
            } else {
                productDetailsWrapper = null;
            }
        }catch (Exception e){
            productDetailsWrapper = null;
        }
        return productDetailsWrapper;
    }

    public ProductListWrapper fetchProductList(int secondCategory,
                                               ArrayList<Integer> sellerIds,
                                               ArrayList<Integer> brandIds,
                                               ArrayList<Integer> colorIds,
                                               int minPrice,
                                               int maxPrice,
                                               String sortBy,
                                               int pageNo
                                               ) {
        ProductListWrapper productListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.getProductsWithoutFilter(secondCategory, sellerIds, brandIds, colorIds, minPrice, maxPrice, sortBy, pageNo);
            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    productListWrapper = null;
                }else{
                    productListWrapper = gson.fromJson(responseBody, ProductListWrapper.class);
                }
            } else {
                productListWrapper = null;
            }
        }catch (Exception e){
            productListWrapper = null;
        }
        return productListWrapper;
    }

    public CommonResponse signUp(String name,
                                         String email,
                                         String password,
                                         String confirmPassword,
                                         String mobile) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();


        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> register = mRepository.userSignUp(name, email, password, confirmPassword, mobile);

            Response<ResponseBody> response = register.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public UserWrapper checkLogin(String email, String password) {
        UserWrapper userWrapper;
        String responseBody = "";
        Gson gson = new Gson();


        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> check = mRepository.checkLogin(email, password);

            Response<ResponseBody> response = check.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userWrapper = null;
                }else{
                    userWrapper = gson.fromJson(responseBody, UserWrapper.class);
                }
            } else {
                userWrapper = null;
            }
        }catch (Exception e){
            userWrapper = null;
        }
        return userWrapper;
    }

    public CommonResponse addToCart(String authorization,
                                    int userId,
                                    int productId,
                                    int quantity,
                                    int colorId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addToCart("Bearer " + authorization, userId, productId, quantity, colorId);

            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CartsWrapper fetchCartDetails(int userId, String apiToken) {
        CartsWrapper cartDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchCartDetails("Bearer " + apiToken, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    cartDetailsWrapper = null;
                }else{
                    cartDetailsWrapper = gson.fromJson(responseBody, CartsWrapper.class);
                }
            } else {
                cartDetailsWrapper = null;
            }
        }catch (Exception e){
            cartDetailsWrapper = null;
        }
        return cartDetailsWrapper;
    }

    public StateListWrapper fetchStateList() {
        StateListWrapper stateListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> fetch = mRepository.fetchStates();

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    stateListWrapper = null;
                }else{
                    stateListWrapper = gson.fromJson(responseBody, StateListWrapper.class);
                }
            } else {
                stateListWrapper = null;
            }
        }catch (Exception e){
            stateListWrapper = null;
        }
        return stateListWrapper;
    }

    public CitiesWrapper fetchCities(int stateId) {
        CitiesWrapper citiesWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> fetch = mRepository.fetchCities(stateId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    citiesWrapper = null;
                }else{
                    citiesWrapper = gson.fromJson(responseBody, CitiesWrapper.class);
                }
            } else {
                citiesWrapper = null;
            }
        }catch (Exception e){
            citiesWrapper = null;
        }
        return citiesWrapper;
    }

    public CommonResponse addShippingAddress(String authorization,
                                             int userId,
                                             String email,
                                             String mobile,
                                             int state,
                                             int city,
                                             String pin,
                                             String address) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addShippingAddress("Bearer " + authorization, userId, email, mobile, state, city, pin, address);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public AddressListWrapper fetchShippingAddressList(int userId, String apiToken) {
        AddressListWrapper addressListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> fetch = mRepository.fetchShippingAddressList("Bearer " + apiToken, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    addressListWrapper = null;
                }else{
                    addressListWrapper = gson.fromJson(responseBody, AddressListWrapper.class);
                }
            } else {
                addressListWrapper = null;
            }
        }catch (Exception e){
            addressListWrapper = null;
        }
        return addressListWrapper;
    }

    public AddressDetailsWrapper getShippingAddressDetails(String apiToken, int addressId) {
        AddressDetailsWrapper addressDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> get = mRepository.getShippingAddressDetails("Bearer " + apiToken, addressId);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    addressDetailsWrapper = null;
                }else{
                    addressDetailsWrapper = gson.fromJson(responseBody, AddressDetailsWrapper.class);
                }
            } else {
                addressDetailsWrapper = null;
            }
        }catch (Exception e){
            addressDetailsWrapper = null;
        }
        return addressDetailsWrapper;
    }

    public CommonResponse updateShippingAddress(String authorization,
                                                int shippingAddressId,
                                                String email,
                                                String mobile,
                                                int state,
                                                int city,
                                                String pin,
                                                String address) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.updateShippingAddress("Bearer " + authorization, shippingAddressId, email, mobile, state, city, pin, address);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse deleteShippingAddress(String authorization,
                                                int shippingAddressId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.deleteShippingAddress("Bearer " + authorization, shippingAddressId);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse deleteItemFromCart(String authorization,
                                                int shippingAddressId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.deleteCartItem("Bearer " + authorization, shippingAddressId);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse updateCart(String authorization,
                                             int quanitity,
                                                int cartId
                                                ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> add = mRepository.updateCart("Bearer " + authorization, quanitity, cartId);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public OrderPlaceDataResponse placeOrder(String authorization, int userId, int paymentMethod, int addressId) {
        OrderPlaceDataResponse orderPlaceDataResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> place = mRepository.placeOrder("Bearer " + authorization, userId, paymentMethod, addressId);
            Response<ResponseBody> response = place.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    orderPlaceDataResponse = null;
                }else{
                    orderPlaceDataResponse = gson.fromJson(responseBody, OrderPlaceDataResponse.class);
                }
            } else {
                orderPlaceDataResponse = null;
            }
        }catch (Exception e){
            orderPlaceDataResponse = null;
        }
        return orderPlaceDataResponse;
    }

    public CommonResponse setPaymentTransactionId(String apiToken, int orderId, String paymentRequestId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> set = mRepository.setPaymentTransactionId("Bearer " + apiToken, orderId, paymentRequestId);
            Response<ResponseBody> response = set.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse updateTransactionId(String apiToken, int orderId, String paymentRequestId, String paymentId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> update = mRepository.updatePaymentTransactionId("Bearer " + apiToken, orderId, paymentRequestId, paymentId);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public OrderListWrapper getOrderHistory(String apiToken, int userId) {
        OrderListWrapper orderListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> get = mRepository.getOrderHistory("Bearer " + apiToken, userId);
            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    orderListWrapper = null;
                }else{
                    orderListWrapper = gson.fromJson(responseBody, OrderListWrapper.class);
                }
            } else {
                orderListWrapper = null;
            }
        }catch (Exception e){
            orderListWrapper = null;
        }
        return orderListWrapper;
    }

    public CommonResponse cancelOrder(String apiToken, int orderId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> cancel = mRepository.cancelOrder("Bearer " + apiToken, orderId);
            Response<ResponseBody> response = cancel.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public UserDetailsWrapper fetchUserInfo(int userId, String apiToken) {
        UserDetailsWrapper userWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> fetch = mRepository.fetchUserProfile("Bearer " + apiToken, userId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userWrapper = null;
                }else{
                    userWrapper = gson.fromJson(responseBody, UserDetailsWrapper.class);
                }
            } else {
                userWrapper = null;
            }
        }catch (Exception e){
            userWrapper = null;
        }
        return userWrapper;
    }

    public CommonResponse updateUserProfile(String authorization,
                                            int userId,
                                            String gender,
                                            String name,
                                            String mobile,
                                            int state,
                                            int city,
                                            String pin,
                                            String address) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> update = mRepository.updateUserProfile("Bearer " + authorization, userId, gender, name, mobile, state, city, pin, address);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

}
