package in.net.webinfotech.udaandemo.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderListWrapper {

    @SerializedName("status")
    @Expose
    public Boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("login_error")
    @Expose
    public int login_error = 0;

    @SerializedName("data")
    @Expose
    public OrderDetails orderDetails;

}
