package in.net.webinfotech.udaandemo.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 02-09-2019.
 */

public class FilterData {

    @SerializedName("second_category")
    @Expose
    public Category[] filterCategories;

    @SerializedName("sellers")
    @Expose
    public Sellers[] sellers;

    @SerializedName("brands")
    @Expose
    public Brands[] brands;

    @SerializedName("colors")
    @Expose
    public Colors[] colors;

}
