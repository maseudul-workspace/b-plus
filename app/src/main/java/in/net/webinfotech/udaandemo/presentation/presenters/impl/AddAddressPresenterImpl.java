package in.net.webinfotech.udaandemo.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.udaandemo.AndroidApplication;
import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.AddAddressInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.FetchCitiesInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.FetchStateListInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.impl.AddAddressInteractorImpl;
import in.net.webinfotech.udaandemo.domain.interactors.impl.FetchCitiesInteractorImpl;
import in.net.webinfotech.udaandemo.domain.interactors.impl.FetchStateListInteractorImpl;
import in.net.webinfotech.udaandemo.domain.model.Cities;
import in.net.webinfotech.udaandemo.domain.model.StateList;
import in.net.webinfotech.udaandemo.domain.model.User;
import in.net.webinfotech.udaandemo.presentation.presenters.AddAddressPresenter;
import in.net.webinfotech.udaandemo.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.CityListDialogAdapter;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.StateListDialogAdapter;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;

public class AddAddressPresenterImpl extends AbstractPresenter implements AddAddressPresenter,
                                                                            FetchStateListInteractor.Callback,
                                                                            StateListDialogAdapter.Callback,
                                                                            FetchCitiesInteractor.Callback,
                                                                            CityListDialogAdapter.Callback,
                                                                            AddAddressInteractor.Callback {

    Context mContext;
    AddAddressPresenter.View mView;
    FetchStateListInteractorImpl fetchStateListInteractor;
    StateListDialogAdapter stateListDialogAdapter;
    FetchCitiesInteractorImpl fetchCitiesInteractor;
    int stateId = 0;
    CityListDialogAdapter cityListDialogAdapter;
    AndroidApplication androidApplication;
    AddAddressInteractorImpl addAddressInteractor;
    int cityId = 0;

    public AddAddressPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void onAddAddressSuccess() {
        Toast.makeText(mContext, "Address Saved Successfully", Toast.LENGTH_SHORT).show();
        mView.hideLoader();
        mView.onAddAddressSuccess();
    }

    @Override
    public void onAddAddressFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.goToLoginActivity();
        } else {
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onGettingCitiesSuccess(Cities[] cities) {
        if (cities.length == 0) {
            mView.hideCityRecyclerView();
        } else {
            cityListDialogAdapter = new CityListDialogAdapter(mContext, cities, this::onCityClicked);
            mView.loadCitiesAdapter(cityListDialogAdapter);
        }
    }

    @Override
    public void onGettingCitiesFail() {
        mView.hideCityRecyclerView();
    }

    @Override
    public void onGettingStateListSuccess(StateList[] states) {
        if (states.length == 0) {
            mView.hideStateRecyclerView();
        } else {
            stateListDialogAdapter = new StateListDialogAdapter(mContext, states, this::onStateClicked);
            mView.loadStateAdapter(stateListDialogAdapter);
        }
    }

    @Override
    public void onGettingStateListFail() {
        mView.hideStateRecyclerView();
    }

    @Override
    public void fetchStateList() {
        fetchStateListInteractor = new FetchStateListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this);
        fetchStateListInteractor.execute();
    }

    @Override
    public void fetchCityList() {
        fetchCitiesInteractor = new FetchCitiesInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, stateId);
        fetchCitiesInteractor.execute();
    }

    @Override
    public void addAddress(String email, String mobile, String pin, String address) {
        if (stateId == 0) {
            Toast.makeText(mContext, "Please select a state", Toast.LENGTH_SHORT).show();
        } else if (cityId == 0) {
            Toast.makeText(mContext, "Please select a city", Toast.LENGTH_SHORT).show();
        } else {
            androidApplication = (AndroidApplication) mContext.getApplicationContext();
            User userInfo = androidApplication.getUserInfo(mContext);
            addAddressInteractor = new AddAddressInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId, email, mobile, stateId, cityId, pin, address);
            addAddressInteractor.execute();
        }
    }

    @Override
    public void onCityClicked(int id, String city) {
        mView.setCityName(city);
        cityId = id;
    }

    @Override
    public void onStateClicked(int id, String stateName) {
        stateId = id;
        mView.setStateName(stateName);
        fetchCityList();
    }
}
