package in.net.webinfotech.udaandemo.presentation.ui.activities;

import android.app.ActivityOptions;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Pair;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.udaandemo.presentation.presenters.FirstCategoryPresenter;
import in.net.webinfotech.udaandemo.presentation.presenters.impl.FirstCategoryPresenterImpl;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.FirstCategoryAdapter;
import in.net.webinfotech.udaandemo.threading.MainThreadImpl;

public class FirstCategoryActivity extends AppCompatActivity implements FirstCategoryPresenter.View {

    @BindView(R.id.recycler_view_first_category)
    RecyclerView recyclerViewFirstCategory;
    FirstCategoryPresenterImpl mPresenter;
    int id;
    String categoryName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_category);
        ButterKnife.bind(this);
        initialisePresenter();
        id = getIntent().getIntExtra("mainCategoryId", 0);
        categoryName = getIntent().getStringExtra("mainCategoryName");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(categoryName);
        mPresenter.fetchFirstCategory(id);
    }

    public void initialisePresenter() {
        mPresenter = new FirstCategoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadData(FirstCategoryAdapter adapter) {
        recyclerViewFirstCategory.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewFirstCategory.setAdapter(adapter);
    }

    @Override
    public void goToSecondCategory(int id, String name, String image, ImageView imageView) {
        Intent secondCategoryIntent = new Intent(this, SecondCategoryActivity.class);
        secondCategoryIntent.putExtra("firstCategoryName", name);
        secondCategoryIntent.putExtra("firstCategoryId", id);
        secondCategoryIntent.putExtra("firstCategoryImage", image);

        Pair[] pairs = new Pair[1];
        pairs[0] = new Pair<View, String> (imageView, "imageTransition");
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(FirstCategoryActivity.this, pairs);
            startActivity(secondCategoryIntent, options.toBundle());
        } else {
            startActivity(secondCategoryIntent);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
