package in.net.webinfotech.udaandemo.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Order {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("order_id")
    @Expose
    public int orderId;

    @SerializedName("quantity")
    @Expose
    public int quantity;

    @SerializedName("price")
    @Expose
    public double price;

    @SerializedName("total")
    @Expose
    public double total;

    @SerializedName("created_at")
    @Expose
    public String orderDate;

    @SerializedName("p_name")
    @Expose
    public String productName;

    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("payment_method")
    @Expose
    public int paymentMethod;

    @SerializedName("payment_status")
    @Expose
    public int paymentStatus;

    @SerializedName("brand_name")
    @Expose
    public String brandName;

    @SerializedName("status")
    @Expose
    public int status;

}
