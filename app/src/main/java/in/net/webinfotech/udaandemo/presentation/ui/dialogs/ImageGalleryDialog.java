package in.net.webinfotech.udaandemo.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.model.Testing.HomeHeaderSliders;
import in.net.webinfotech.udaandemo.domain.model.Testing.Thumbnails;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.ImageGalleryThumbnailsAdapter;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.ZoomImagesAdapter;

/**
 * Created by Raj on 07-05-2019.
 */

public class ImageGalleryDialog implements ImageGalleryThumbnailsAdapter.Callback{
    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    RecyclerView recyclerViewThumbnails;
    ImageGalleryThumbnailsAdapter adapter;
    ArrayList<Thumbnails> thumbnailsArrayList;
    ArrayList<HomeHeaderSliders> slidersArrayList;
    ViewPager viewPager;

    public ImageGalleryDialog(Context mContext, Activity mActivity, ArrayList<HomeHeaderSliders> slidersArrayList)
    {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.slidersArrayList = slidersArrayList;

    }

    public void setUpDialog(){
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_image_gallery, null);
        viewPager = (ViewPager) dialogContainer.findViewById(R.id.image_gallery_viewpager);
        recyclerViewThumbnails = (RecyclerView) dialogContainer.findViewById(R.id.image_gallery_recycler_view);
        ZoomImagesAdapter slidingImagesAdapter = new ZoomImagesAdapter(mContext, slidersArrayList);
        viewPager.setAdapter(slidingImagesAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                resetThumbnails();
                thumbnailsArrayList.get(position).isSelected = true;
                adapter.updateThumbnils(thumbnailsArrayList);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        setThumbnailAdapter();
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
    }

    public void setThumbnailAdapter(){
        thumbnailsArrayList = new ArrayList<>();
        for(int i = 0; i < slidersArrayList.size(); i++){
            if(i == 0){
                Thumbnails thumbnails = new Thumbnails(slidersArrayList.get(i).imageUrl, true);
                thumbnailsArrayList.add(thumbnails);
            }else{
                Thumbnails thumbnails = new Thumbnails(slidersArrayList.get(i).imageUrl, false);
                thumbnailsArrayList.add(thumbnails);
            }
        }
        adapter = new ImageGalleryThumbnailsAdapter(mContext, thumbnailsArrayList, this);
        recyclerViewThumbnails.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        recyclerViewThumbnails.setAdapter(adapter);
    }

    public void showDialog(int position){
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        viewPager.setCurrentItem(position);
        dialog.show();
    }

    public void resetThumbnails(){
        for(int i = 0; i < thumbnailsArrayList.size(); i++){
            thumbnailsArrayList.get(i).isSelected = false;
        }
    }

    @Override
    public void onThumbnailClicked(int position) {
       viewPager.setCurrentItem(position, true);
    }
}
