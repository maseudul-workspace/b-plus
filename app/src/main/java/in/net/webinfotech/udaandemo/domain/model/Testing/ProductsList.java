package in.net.webinfotech.udaandemo.domain.model.Testing;

/**
 * Created by Raj on 26-04-2019.
 */

public class ProductsList {
    public String productName;
    public String imageUrl;
    public double price;


    public ProductsList(String productName, String imageUrl, double price) {
        this.productName = productName;
        this.imageUrl = imageUrl;
        this.price = price;
    }
}
