package in.net.webinfotech.udaandemo.domain.interactors;

public interface DeleteItemFromCartInteractor {
    interface Callback {
        void onCartItemDeleteSuccess();
        void onCartItemDeleteFail(String errorMsg, int loginError);
    }
}
