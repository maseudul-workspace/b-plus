package in.net.webinfotech.udaandemo.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.model.Sellers;

/**
 * Created by Raj on 02-09-2019.
 */

public class SellersFilterAdapter extends RecyclerView.Adapter<SellersFilterAdapter.ViewHolder> {

    public interface Callback {
        void onSellerSelect(int sellerId);
        void onSellerNotSelect(int sellerId);
    }

    Context mContext;
    Sellers[] sellers;
    Callback mCallback;

    public SellersFilterAdapter(Context mContext, Sellers[] sellers, Callback callback) {
        this.mContext = mContext;
        this.sellers = sellers;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_filter_by, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.checkBox.setText(sellers[i].sellerName);
        viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mCallback.onSellerSelect(sellers[i].sellerId);
                } else {
                    mCallback.onSellerNotSelect(sellers[i].sellerId);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return sellers.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.recycler_view_filter_by_checkbox)
        CheckBox checkBox;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
