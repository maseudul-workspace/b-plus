package in.net.webinfotech.udaandemo.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.udaandemo.AndroidApplication;
import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.FetchShippingAddressListInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.PlaceOrderInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.impl.FetchShippingAddressListInteractorImpl;
import in.net.webinfotech.udaandemo.domain.interactors.impl.PlaceOrderInteractorImpl;
import in.net.webinfotech.udaandemo.domain.model.Address;
import in.net.webinfotech.udaandemo.domain.model.OrderPlaceData;
import in.net.webinfotech.udaandemo.domain.model.User;
import in.net.webinfotech.udaandemo.presentation.presenters.DeliveryAddressPresenter;
import in.net.webinfotech.udaandemo.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.DeliveryAddressAdapter;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;

public class DeliveryAddressPresenterImpl extends AbstractPresenter implements DeliveryAddressPresenter,
                                                                                FetchShippingAddressListInteractor.Callback,
                                                                                DeliveryAddressAdapter.Callback,
                                                                                PlaceOrderInteractor.Callback

{

    Context mContext;
    DeliveryAddressPresenter.View mView;
    AndroidApplication androidApplication;
    FetchShippingAddressListInteractorImpl fetchShippingAddressListInteractor;
    DeliveryAddressAdapter deliveryAddressAdapter;
    Address[] addresses;
    PlaceOrderInteractorImpl placeOrderInteractor;

    public DeliveryAddressPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchShippingAddress() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User userInfo = androidApplication.getUserInfo(mContext);
        fetchShippingAddressListInteractor = new FetchShippingAddressListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.userId, userInfo.apiToken);
        fetchShippingAddressListInteractor.execute();
    }

    @Override
    public void placeOrder(int addressId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        User user = androidApplication.getUserInfo(mContext);
        placeOrderInteractor = new PlaceOrderInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, 2, addressId);
        placeOrderInteractor.execute();
    }

    @Override
    public void onGettingShippingAddressSuccess(Address[] addresses) {
        this.addresses = addresses;
        addresses[0].isSelected = true;
        deliveryAddressAdapter = new DeliveryAddressAdapter(mContext, addresses, this);
        mView.loadAdapter(deliveryAddressAdapter);
        mView.hideLoader();
        mView.onAddressSelected(addresses[0].id);
    }

    @Override
    public void onGettingShippingAddressFail(String errorMsg, int isLoginError) {
        mView.hideLoader();
        if (isLoginError == 1) {
            mView.goToLoginActivity();
        } else {
            mView.hideAddressRecyclerView();
        }
    }

    @Override
    public void onEditClicked(int id) {

    }

    @Override
    public void onAddressSelected(int id) {
        for (int i = 0; i < this.addresses.length; i++) {
            if (this.addresses[i].id == id) {
                this.addresses[i].isSelected = true;
                mView.onAddressSelected(id);
            } else {
                this.addresses[i].isSelected = false;
            }
        }
        deliveryAddressAdapter.updateDataset(this.addresses);
    }

    @Override
    public void onPlaceOrderSuccess(OrderPlaceData orderPlaceData) {
        mView.hideLoader();
        mView.onOrderPlaceSuccess(orderPlaceData.orderId);
    }

    @Override
    public void onPlaceOrderFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.goToLoginActivity();
        } else {
            Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }
}
