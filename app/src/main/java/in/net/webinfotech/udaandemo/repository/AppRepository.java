package in.net.webinfotech.udaandemo.repository;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Raj on 28-08-2019.
 */

public interface AppRepository {

    @GET("app/load")
    Call<ResponseBody> fetchHomeData();

    @GET("first_category/{Main_category_id}")
    Call<ResponseBody> getSubCategories(@Path("Main_category_id") int id);

    @GET("second/category/{first_category_id}")
    Call<ResponseBody> getSecondCategories(@Path("first_category_id") int id);

    @GET("seller/list/{second_category}/{page_no}")
    Call<ResponseBody> getProductsBySellers(@Path("second_category") int id,
                                           @Path("page_no") int pageNo);

    @GET("product/list/{second_category}/{seller_id}/{page_no}")
    Call<ResponseBody> getProductsListWithFilters(@Path("second_category") int id,
                                                  @Path("seller_id") int sellerId,
                                                  @Path("page_no") int pageNo);

    @POST("product/filter")
    @FormUrlEncoded
    Call<ResponseBody> getProductsWithoutFilter(@Field("second_category") int secondCategory,
                                                @Field("sellers_id[]") ArrayList<Integer> sellerIds,
                                                @Field("brands_id[]") ArrayList<Integer> brandIds,
                                                @Field("colors_id[]") ArrayList<Integer> colorIds,
                                                @Field("min_price") int minPrice,
                                                @Field("max_price") int maxPrice,
                                                @Field("sort_by") String sortBy,
                                                @Field("page_no") int pageNo
                                                );

    @POST("user/create")
    @FormUrlEncoded
    Call<ResponseBody> userSignUp(@Field("name") String name,
                                    @Field("email") String email,
                                    @Field("password") String password,
                                    @Field("confirm_password") String confirmPassword,
                                    @Field("mobile") String mobile
    );

    @POST("user/login")
    @FormUrlEncoded
    Call<ResponseBody> checkLogin(@Field("email") String email,
                                  @Field("password") String password
    );

    @GET("product/single/view/{product_id}")
    Call<ResponseBody> getProductDetails(@Path("product_id") int product_id);

    @POST("cart/add")
    @FormUrlEncoded
    Call<ResponseBody> addToCart(@Header("Authorization") String authorization,
                                 @Field("user_id") int userId,
                                 @Field("product_id") int productId,
                                 @Field("quantity") int quantity,
                                 @Field("color_id") int colorId);

    @GET("cart/products/{user_id}")
    Call<ResponseBody> fetchCartDetails(@Header("Authorization") String authorization,
                                        @Path("user_id") int userId
    );

    @GET("state_list")
    Call<ResponseBody> fetchStates();


    @GET("city/list/{state_id}")
    Call<ResponseBody> fetchCities(@Path("state_id") int stateId);

    @POST("user/shipping/address/add")
    @FormUrlEncoded
    Call<ResponseBody> addShippingAddress(@Header("Authorization") String authorization,
                                          @Field("user_id") int userId,
                                          @Field("email") String email,
                                          @Field("mobile") String mobile,
                                          @Field("state") int state,
                                          @Field("city") int city,
                                          @Field("pin") String pin,
                                          @Field("address") String address
    );

    @GET("user/shipping/address/list/{user_id}")
    Call<ResponseBody> fetchShippingAddressList(@Header("Authorization") String authorization,
                                                @Path("user_id") int userId
    );

    @GET("user/shipping/address/{address_id}")
    Call<ResponseBody> getShippingAddressDetails(@Header("Authorization") String authorization,
                                                 @Path("address_id") int addressId
    );

    @GET("user/shipping/address/delete/{address_id}")
    Call<ResponseBody> deleteShippingAddress(@Header("Authorization") String authorization,
                                                 @Path("address_id") int addressId
    );

    @GET("cart/delete/{cart_id}")
    Call<ResponseBody> deleteCartItem(@Header("Authorization") String authorization,
                                             @Path("cart_id") int cartId
    );

    @POST("user/shipping/update")
    @FormUrlEncoded
    Call<ResponseBody> updateShippingAddress(@Header("Authorization") String authorization,
                                             @Field("shipping_address_id") int shippingAddressId,
                                             @Field("email") String email,
                                             @Field("mobile") String mobile,
                                             @Field("state") int state,
                                             @Field("city") int city,
                                             @Field("pin") String pin,
                                             @Field("address") String address
    );

    @POST("cart/update")
    @FormUrlEncoded
    Call<ResponseBody> updateCart(@Header("Authorization") String authorization,
                                  @Field("quantity") int qty,
                                  @Field("cart_id") int cartId
    );

    @POST("place/order")
    @FormUrlEncoded
    Call<ResponseBody> placeOrder(@Header("Authorization") String authorization,
                                  @Field("user_id") int userId,
                                  @Field("payment_method") int paymentMethod,
                                  @Field("address_id") int addressId
    );

    @POST("update/payment/request/id")
    @FormUrlEncoded
    Call<ResponseBody> setPaymentTransactionId(@Header("Authorization") String authorization,
                                  @Field("order_id") int orderId,
                                  @Field("payment_request_id") String paymentRequestId
    );

    @POST("update/payment/id")
    @FormUrlEncoded
    Call<ResponseBody> updatePaymentTransactionId(@Header("Authorization") String authorization,
                                               @Field("order_id") int orderId,
                                               @Field("payment_request_id") String paymentRequestId,
                                               @Field("payment_id") String paymentId

    );

    @GET("order/history/{user_id}")
    Call<ResponseBody> getOrderHistory(@Header("Authorization") String authorization,
                                               @Path("user_id") int userId);

    @GET("order/cancel/{order_details_id}")
    Call<ResponseBody> cancelOrder(@Header("Authorization") String authorization,
                                   @Path("order_details_id") int orderDetailsId);

    @GET("user/profile/{user_id}")
    Call<ResponseBody> fetchUserProfile(@Header("Authorization") String authorization,
                                   @Path("user_id") int userId);

    @POST("user/profile/update")
    @FormUrlEncoded
    Call<ResponseBody> updateUserProfile(@Header("Authorization") String authorization,
                                         @Field("user_id") int userId,
                                         @Field("gender") String gender,
                                         @Field("name") String name,
                                         @Field("mobile") String mobile,
                                         @Field("state") int state,
                                         @Field("city") int city,
                                         @Field("pin") String pin,
                                         @Field("address") String address
    );


}
