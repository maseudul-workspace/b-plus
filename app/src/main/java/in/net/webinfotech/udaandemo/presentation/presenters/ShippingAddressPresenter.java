package in.net.webinfotech.udaandemo.presentation.presenters;


import in.net.webinfotech.udaandemo.presentation.ui.adapters.ShippingAddressAdapter;

public interface ShippingAddressPresenter {
    void fetchShippingAddress();
    interface View {
        void loadAdapter(ShippingAddressAdapter adapter);
        void showLoader();
        void hideLoader();
        void hideAddressRecyclerView();
        void goToAddressEditActivity(int id);
        void goToLoginActivity();
    }
}
