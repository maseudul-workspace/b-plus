package in.net.webinfotech.udaandemo.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.udaandemo.presentation.presenters.AddAddressPresenter;
import in.net.webinfotech.udaandemo.presentation.presenters.impl.AddAddressPresenterImpl;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.CityListDialogAdapter;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.StateListDialogAdapter;
import in.net.webinfotech.udaandemo.presentation.ui.dialogs.SelectCityDialog;
import in.net.webinfotech.udaandemo.presentation.ui.dialogs.SelectStateDialog;
import in.net.webinfotech.udaandemo.threading.MainThreadImpl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

public class AddAddressActivity extends AppCompatActivity implements AddAddressPresenter.View {

    AddAddressPresenterImpl mPresenter;
    SelectStateDialog selectStateDialog;
    @BindView(R.id.txt_view_state)
    TextView txtViewState;
    SelectCityDialog selectCityDialog;
    @BindView(R.id.txt_view_city_name)
    TextView txtViewCityName;
    @BindView(R.id.txt_input_email_layout)
    TextInputLayout txtInputEmailLayout;
    @BindView(R.id.txt_input_phone_layout)
    TextInputLayout txtInputPhoneLayout;
    @BindView(R.id.txt_input_address_layout)
    TextInputLayout txtInputAddressLayout;
    @BindView(R.id.txt_input_pincode_layout)
    TextInputLayout txtInputPincodeLayout;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_pincode)
    EditText editTextPincode;
    @BindView(R.id.edit_text_address)
    EditText editTextAddress;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);
        ButterKnife.bind(this);
        setProgressDialog();
        initialisePresenter();
        setUpStateSelectDialogView();
        setSelectCityDialog();
        mPresenter.fetchStateList();
    }

    public void initialisePresenter() {
        mPresenter = new AddAddressPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    public void setUpStateSelectDialogView() {
        selectStateDialog = new SelectStateDialog(this, this);
        selectStateDialog.setUpDialogView();
    }

    public void setSelectCityDialog() {
        selectCityDialog = new SelectCityDialog(this, this);
        selectCityDialog.setUpDialogView();
    }


    @Override
    public void loadStateAdapter(StateListDialogAdapter stateListDialogAdapter) {
        selectStateDialog.setRecyclerView(stateListDialogAdapter);
    }

    @Override
    public void loadCitiesAdapter(CityListDialogAdapter cityListDialogAdapter) {
        selectCityDialog.setRecyclerView(cityListDialogAdapter);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.hide();
    }

    @Override
    public void setStateName(String stateName) {
        txtViewState.setText(stateName);
        txtViewState.setTextColor(this.getResources().getColor(R.color.black1));
        selectStateDialog.hideDialog();
    }

    @Override
    public void setCityName(String cityName) {
        txtViewCityName.setText(cityName);
        txtViewCityName.setTextColor(this.getResources().getColor(R.color.black1));
        selectCityDialog.hideDialog();
    }

    @Override
    public void hideStateRecyclerView() {
        selectCityDialog.hideRecyclerView();
    }

    @Override
    public void hideCityRecyclerView() {
        selectCityDialog.hideRecyclerView();
    }

    @Override
    public void onAddAddressSuccess() {
        finish();
    }

    @Override
    public void goToLoginActivity() {
        Toast.makeText(this, "Your session expired !! Please login again", Toast.LENGTH_LONG).show();
        Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(loginIntent);
    }

    @OnClick(R.id.state_linear_layout) void onStateClicked() {
        selectStateDialog.showDialog();
    }

    @OnClick(R.id.city_linear_layout) void onCityClicked() {
        selectCityDialog.showDialog();
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (editTextEmail.getText().toString().trim().isEmpty()) {
            txtInputEmailLayout.setError("Email Required");
            editTextEmail.requestFocus();
        } else if (editTextPhone.getText().toString().trim().isEmpty()) {
            txtInputPhoneLayout.setError("Phone Required");
            editTextPhone.requestFocus();
        } else if (editTextPincode.getText().toString().trim().isEmpty()) {
            txtInputPincodeLayout.setError("Pincode Required");
            editTextPincode.requestFocus();
        } else if (editTextAddress.getText().toString().trim().isEmpty()) {
            txtInputAddressLayout.setError("Address Required");
            editTextAddress.requestFocus();
        } else if(!Patterns.EMAIL_ADDRESS.matcher(editTextEmail.getText().toString()).matches()){
            txtInputEmailLayout.setError("Enter a valid email");
            editTextEmail.requestFocus();
        } else {
            mPresenter.addAddress(
                    editTextEmail.getText().toString(),
                    editTextPhone.getText().toString(),
                    editTextPincode.getText().toString(),
                    editTextAddress.getText().toString()
            );
            progressDialog.show();
        }
    }

}
