package in.net.webinfotech.udaandemo.domain.interactors.impl;

import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.GetOrderHistoryInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.udaandemo.domain.model.Order;
import in.net.webinfotech.udaandemo.domain.model.OrderListWrapper;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;

public class GetOrderHistoryInteractorImpl extends AbstractInteractor implements GetOrderHistoryInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;

    public GetOrderHistoryInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingOrderHistoryFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(final Order[] orders){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingOrderHistorySuccess(orders);
            }
        });
    }

    @Override
    public void run() {
        final OrderListWrapper orderListWrapper = mRepository.getOrderHistory(apiToken, userId);
        if (orderListWrapper == null) {
            notifyError(orderListWrapper.message, 0);
        } else if (!orderListWrapper.status) {
            notifyError(orderListWrapper.message, orderListWrapper.login_error);
        } else {
            postMessage(orderListWrapper.orderDetails.orders);
        }
    }
}
