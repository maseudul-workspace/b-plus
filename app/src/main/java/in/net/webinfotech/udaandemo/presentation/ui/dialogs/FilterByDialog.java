package in.net.webinfotech.udaandemo.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import com.google.android.material.tabs.TabLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.model.FilterData;
import in.net.webinfotech.udaandemo.domain.model.Testing.Brands;
import in.net.webinfotech.udaandemo.domain.model.Testing.Sellers;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.BrandsFilterAdapter;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.CategoryFilterAdapter;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.ColorFilterAdapter;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.SellersFilterAdapter;

/**
 * Created by Raj on 29-04-2019.
 */

public class FilterByDialog implements BrandsFilterAdapter.Callback,  CategoryFilterAdapter.Callback, ColorFilterAdapter.Callback, SellersFilterAdapter.Callback {

    public interface Callback {
        void onFiltersApplied( ArrayList<Integer> sellerIds, ArrayList<Integer> brandIds, ArrayList<Integer> colorIds, int minPrice, int maxPrice);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    TabLayout tabLayout;
    ArrayList<Brands> brandsArrayList;
    ArrayList<Sellers> sellersArrayList;
    Button applyFilterBtn;
    View priceFilterLayout;
    RecyclerView recyclerViewCategoryFilter;
    RecyclerView recyclerViewSellerFilter;
    RecyclerView recyclerViewBrandsFilter;
    RecyclerView recyclerViewColorsFilter;
    BrandsFilterAdapter brandsFilterAdapter;
    CategoryFilterAdapter categoryFilterAdapter;
    ColorFilterAdapter colorFilterAdapter;
    SellersFilterAdapter sellersFilterAdapter;
    EditText editTextPriceFrom;
    EditText editTextPriceTo;
    ArrayList<Integer> sellerIds;
    ArrayList<Integer> brandIds;
    ArrayList<Integer> colorIds;
    int minPrice = 0;
    int maxPrice = 0;
    Callback mCallback;

    public FilterByDialog(Context mContext, Activity mActivity, Callback callback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        mCallback = callback;
    }

    public void setUpDialogView(){
        sellerIds = new ArrayList<>();
        brandIds = new ArrayList<>();
        colorIds = new ArrayList<>();
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.filter_by_dialog_layout, null);
        recyclerViewBrandsFilter = (RecyclerView) dialogContainer.findViewById(R.id.recycler_view_brands_filter);
        recyclerViewCategoryFilter = (RecyclerView) dialogContainer.findViewById(R.id.recycler_view_categories_filter);
        recyclerViewColorsFilter = (RecyclerView) dialogContainer.findViewById(R.id.recycler_view_colors_filters);
        recyclerViewSellerFilter = (RecyclerView) dialogContainer.findViewById(R.id.recycler_view_sellers_filter);
        applyFilterBtn = (Button) dialogContainer.findViewById(R.id.btn_apply_filter);
        editTextPriceFrom = (EditText) dialogContainer.findViewById(R.id.edit_text_price_form);
        editTextPriceTo = (EditText) dialogContainer.findViewById(R.id.edit_text_price_to);

        applyFilterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    minPrice = Integer.parseInt(editTextPriceFrom.getText().toString());
                } catch (NumberFormatException e) {
                    minPrice = 0;
                }

                try {
                    maxPrice = Integer.parseInt(editTextPriceTo.getText().toString());
                } catch (NumberFormatException e) {
                    maxPrice = 0;
                }
                if (minPrice > maxPrice) {
                    Toast.makeText(mContext, "Minimun price should be less than maximun price", Toast.LENGTH_SHORT).show();
                } else {
                    mCallback.onFiltersApplied(sellerIds, brandIds, colorIds, minPrice, maxPrice);
                    dialog.dismiss();
                }
            }
        });

        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
    }

    public void setData(FilterData filterData) {

        brandsFilterAdapter = new BrandsFilterAdapter(mContext, filterData.brands, this);
        categoryFilterAdapter = new CategoryFilterAdapter(mContext, filterData.filterCategories, this);
        colorFilterAdapter = new ColorFilterAdapter(mContext, filterData.colors, this);
        sellersFilterAdapter = new SellersFilterAdapter(mContext, filterData.sellers, this);

        recyclerViewBrandsFilter.setAdapter(brandsFilterAdapter);
        recyclerViewBrandsFilter.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerViewBrandsFilter.setNestedScrollingEnabled(false);

        recyclerViewSellerFilter.setAdapter(sellersFilterAdapter);
        recyclerViewSellerFilter.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerViewSellerFilter.setNestedScrollingEnabled(false);

        recyclerViewColorsFilter.setAdapter(colorFilterAdapter);
        recyclerViewColorsFilter.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerViewColorsFilter.setNestedScrollingEnabled(false);

        recyclerViewCategoryFilter.setAdapter(categoryFilterAdapter);
        recyclerViewCategoryFilter.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerViewCategoryFilter.setNestedScrollingEnabled(false);
    }

    public void showDialog(){
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.show();
    }

    @Override
    public void onBrandsSelect(int brandId) {
        brandIds.add(brandId);
    }

    @Override
    public void onBrandsNotSelect(int brandId) {
        for (int i = 0; i < brandIds.size(); i++) {
            if (brandIds.get(i) == brandId) {
                brandIds.remove(i);
                break;
            }
        }
    }

    @Override
    public void onCategoryClicked(int categoryId) {

    }

    @Override
    public void onColorSelect(int colorId) {
        colorIds.add(colorId);
    }

    @Override
    public void onColorNotSelect(int colorId) {
        for (int i = 0; i < colorIds.size(); i++) {
            if (colorIds.get(i) == colorId) {
                colorIds.remove(i);
                break;
            }
        }
    }

    @Override
    public void onSellerSelect(int sellerId) {
        sellerIds.add(sellerId);
    }

    @Override
    public void onSellerNotSelect(int sellerId) {
        for (int i = 0; i < sellerIds.size(); i++) {
            if (sellerIds.get(i) == sellerId) {
                sellerIds.remove(i);
                break;
            }
        }
    }
}
