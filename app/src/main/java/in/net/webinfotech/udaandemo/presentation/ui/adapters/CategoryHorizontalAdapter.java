package in.net.webinfotech.udaandemo.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.model.MainCategory;
import in.net.webinfotech.udaandemo.util.GlideHelper;

/**
 * Created by Raj on 25-04-2019.
 */

public class CategoryHorizontalAdapter extends RecyclerView.Adapter<CategoryHorizontalAdapter.ViewHolder> {

    public interface Callback {
        void onCategoryClicked(int id, String name);
    }

    Context mContext;
    MainCategory[] mainCategories;
    Callback mCallback;

    public CategoryHorizontalAdapter(Context mContext, MainCategory[] mainCategories, Callback callback) {
        this.mContext = mContext;
        this.mainCategories = mainCategories;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_categories_horizontal, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.txtViewCategory.setText(mainCategories[position].name);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewCategoryIcon, mContext.getResources().getString(R.string.api_url) + "main/category/image/" + mainCategories[position].image, 100);
        holder.imgViewCategoryIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onCategoryClicked(mainCategories[position].id, mainCategories[position].name);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mainCategories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

       @BindView(R.id.img_view_category_icon)
       ImageView imgViewCategoryIcon;
       @BindView(R.id.txt_view_category)
       TextView txtViewCategory;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
