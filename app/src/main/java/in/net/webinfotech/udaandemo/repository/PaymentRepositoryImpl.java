package in.net.webinfotech.udaandemo.repository;

import android.util.Log;

import com.google.gson.Gson;

import in.net.webinfotech.udaandemo.domain.model.AccessTokenResponse;
import in.net.webinfotech.udaandemo.domain.model.GatewayOrderResponse;
import in.net.webinfotech.udaandemo.domain.model.PaymentRequestResponse;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.Header;

public class PaymentRepositoryImpl {

    PaymentRepository mRepository;

    public PaymentRepositoryImpl() {
        mRepository = PaymentAPIclient.createService(PaymentRepository.class);
    }

    public AccessTokenResponse fetchAccessToken(String clientId, String clientSecret, String grantType) {
        AccessTokenResponse accessTokenResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.getAccessToken(clientId, clientSecret, grantType);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    accessTokenResponse = null;
                }else{
                    accessTokenResponse = gson.fromJson(responseBody, AccessTokenResponse.class);
                }
            } else {
                accessTokenResponse = null;
            }
        }catch (Exception e){
            accessTokenResponse = null;
        }
        return accessTokenResponse;
    }

    public GatewayOrderResponse initiateOrder(String authorization, String name, String email, String phone, String amount, String description, String currency, String transaction_id, String redirect_url, String env) {
        GatewayOrderResponse gatewayOrderResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.initiateOrder("Bearer " + authorization, name, email, phone, amount, description, currency, transaction_id, redirect_url, env);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    gatewayOrderResponse = null;
                }else{
                    gatewayOrderResponse = gson.fromJson(responseBody, GatewayOrderResponse.class);
                }
            } else {
                gatewayOrderResponse = null;
            }
        }catch (Exception e){
            gatewayOrderResponse = null;
        }
        return gatewayOrderResponse;
    }

    public PaymentRequestResponse requestPayment(String authorization, String id) {
        PaymentRequestResponse paymentRequestResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.requestPayment("Bearer " + authorization, id);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    paymentRequestResponse = null;
                }else{
                    paymentRequestResponse = gson.fromJson(responseBody, PaymentRequestResponse.class);
                }
            } else {
                paymentRequestResponse = null;
            }
        }catch (Exception e){
            paymentRequestResponse = null;
        }
        return paymentRequestResponse;
    }

}
