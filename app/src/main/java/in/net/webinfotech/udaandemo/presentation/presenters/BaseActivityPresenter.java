package in.net.webinfotech.udaandemo.presentation.presenters;

public interface BaseActivityPresenter {
    void fetchCartCount();
    interface View {
        void loadCartCount(int count);
    }
}
