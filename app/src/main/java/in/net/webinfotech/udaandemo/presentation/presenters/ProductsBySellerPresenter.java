package in.net.webinfotech.udaandemo.presentation.presenters;

import in.net.webinfotech.udaandemo.presentation.ui.adapters.SellerProductListAdapter;

/**
 * Created by Raj on 29-08-2019.
 */

public interface ProductsBySellerPresenter {

    void fetchProductsBySeller(int id, int pageNo, String type);

    interface View {
        void loadAdapter(SellerProductListAdapter adapter, int totalPage);
        void showLoader();
        void hideLoader();
        void goToProductList(int subcategoryId, int sellerId);
        void goToProductDetails(int productId);
        void hidePaginationProgressBar();
        void showPaginationProgressBar();
    }
}
