package in.net.webinfotech.udaandemo.presentation.ui.activities;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.MenuItem;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.udaandemo.presentation.presenters.SecondCategoryPresenter;
import in.net.webinfotech.udaandemo.presentation.presenters.impl.SecondCategoryPresenterImpl;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.SecondCategoryAdapter;
import in.net.webinfotech.udaandemo.threading.MainThreadImpl;
import in.net.webinfotech.udaandemo.util.GlideHelper;

public class SecondCategoryActivity extends AppCompatActivity implements SecondCategoryPresenter.View {

    @BindView(R.id.recycler_view_second_category)
    RecyclerView recyclerViewSecondCategory;
    @BindView(R.id.img_view_first_category)
    ImageView imgViewFirstCategory;
    int id;
    String firstCategoryImage;
    String firstCategoryName;
    SecondCategoryPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_category);
        ButterKnife.bind(this);

        firstCategoryImage = getIntent().getStringExtra("firstCategoryImage");
        id = getIntent().getIntExtra("firstCategoryId", 0);
        firstCategoryName = getIntent().getStringExtra("firstCategoryName");
        getSupportActionBar().setTitle(firstCategoryName);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        GlideHelper.setImageView(this, imgViewFirstCategory, getResources().getString(R.string.api_url) + "first/category/image/" + firstCategoryImage);
        initialisePresenter();
        mPresenter.fetchSecondCategories(id);
    }

    public void initialisePresenter() {
        mPresenter = new SecondCategoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadAdapter(SecondCategoryAdapter adapter) {
        recyclerViewSecondCategory.setAdapter(adapter);
        recyclerViewSecondCategory.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerViewSecondCategory.setNestedScrollingEnabled(false);
    }

    @Override
    public void goToProductsBySellerList(int id) {
        Intent intent = new Intent(this, ProductsBySellerActivity.class);
        intent.putExtra("categoryId", id);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
