package in.net.webinfotech.udaandemo.domain.executor;

/**
 * Created by Raj on 24-04-2019.
 */

public interface MainThread {
    /**
     * Make runnable operation run in the main thread.
     *
     * @param runnable The runnable to run.
     */
    void post(final Runnable runnable);
}
