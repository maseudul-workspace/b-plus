package in.net.webinfotech.udaandemo.domain.interactors;

import in.net.webinfotech.udaandemo.domain.model.Order;

public interface GetOrderHistoryInteractor {
    interface Callback {
        void onGettingOrderHistorySuccess(Order[] orders);
        void onGettingOrderHistoryFail(String errorMsg, int loginError);
    }
}
