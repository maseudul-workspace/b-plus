package in.net.webinfotech.udaandemo.domain.interactors.impl;

import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.AddToCartInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.udaandemo.domain.model.CommonResponse;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;

public class AddToCartInteractorImpl extends AbstractInteractor implements AddToCartInteractor {

    AppRepositoryImpl appRepository;
    Callback mCallback;
    String authorization;
    int userId;
    int productId;
    int quantity;
    int colorId;

    public AddToCartInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl appRepository, Callback mCallback, String authorization, int userId, int productId, int quantity, int colorId) {
        super(threadExecutor, mainThread);
        this.appRepository = appRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.userId = userId;
        this.productId = productId;
        this.quantity = quantity;
        this.colorId = colorId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddToCartFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddToCartSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = appRepository.addToCart(authorization, userId, productId, quantity, colorId);
        if (commonResponse == null) {
            notifyError("Something went wrong", commonResponse.loginError);
        } else if(!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.loginError);
        } else {
            postMessage();
        }
    }
}
