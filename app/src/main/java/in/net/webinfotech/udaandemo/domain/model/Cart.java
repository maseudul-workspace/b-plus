package in.net.webinfotech.udaandemo.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Cart {

    @SerializedName("cart_id")
    @Expose
    public int cartId;

    @SerializedName("product_id")
    @Expose
    public int productId;

    @SerializedName("color_id")
    @Expose
    public int colorId;

    @SerializedName("quantity")
    @Expose
    public int quantity;

    @SerializedName("product_name")
    @Expose
    public String productName;

    @SerializedName("min_ord_qtty")
    @Expose
    public int minOrderQty;

    @SerializedName("product_price")
    @Expose
    public double productPrice;

    @SerializedName("product_mrp")
    @Expose
    public double productMRP;

    @SerializedName("product_tag_name")
    @Expose
    public String productTagName;

    @SerializedName("product_image")
    @Expose
    public String productImage;

    @SerializedName("color_name")
    @Expose
    public String colorName;

    @SerializedName("color_value")
    @Expose
    public String colorValue;


}
