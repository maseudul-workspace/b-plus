package in.net.webinfotech.udaandemo.domain.interactors;

import in.net.webinfotech.udaandemo.domain.model.Cart;

public interface FetchCartsInteractor {
    interface Callback {
        void onGettingCartDetailsSuccess(Cart[] carts);
        void onGettingCartDetailsFail(String errorMsg, int loginError);
    }
}
