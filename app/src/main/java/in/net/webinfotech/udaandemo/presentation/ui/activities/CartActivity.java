package in.net.webinfotech.udaandemo.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.udaandemo.presentation.presenters.CartsPresenter;
import in.net.webinfotech.udaandemo.presentation.presenters.impl.CartsPresenterImpl;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.CartAdapter;
import in.net.webinfotech.udaandemo.threading.MainThreadImpl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class CartActivity extends AppCompatActivity implements CartsPresenter.View {

    @BindView(R.id.recycler_view_cart)
    RecyclerView recyclerViewCart;
    @BindView(R.id.txt_view_cart_total)
    TextView txtViewCartTotal;
    @BindView(R.id.bottom_layout)
    View bottomLayout;
    CartsPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    String amount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
        getSupportActionBar().setTitle("My Cart");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void initialisePresenter() {
        mPresenter = new CartsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.hide();
    }

    @Override
    public void loadData(CartAdapter adapter, double cartTotal) {
        recyclerViewCart.setVisibility(View.VISIBLE);
        bottomLayout.setVisibility(View.VISIBLE);
        txtViewCartTotal.setText(Double.toString(cartTotal));
        recyclerViewCart.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewCart.setAdapter(adapter);
        amount = Double.toString(cartTotal);
    }

    @Override
    public void hideCartRecyclerView() {
        recyclerViewCart.setVisibility(View.GONE);
        bottomLayout.setVisibility(View.GONE);
    }

    @Override
    public void goToLoginActivity() {
        Toast.makeText(this, "Your session expired !! Please login again", Toast.LENGTH_LONG).show();
        Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(loginIntent);
    }

    @OnClick(R.id.btn_select_address) void onSelectAddressClicked() {
        Intent intent = new Intent(this, DeliveryAddressActivity.class);
        intent.putExtra("amount", amount);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchCartProducts();
        showLoader();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
