package in.net.webinfotech.udaandemo.domain.interactors;

import in.net.webinfotech.udaandemo.domain.model.PaymentRequestResponse;

public interface RequestPaymentInteractor {
    interface Callback {
        void onRequestPaymentSuccess(PaymentRequestResponse paymentRequestResponse);
        void onRequestPaymentFail(String errorMsg);
    }
}
