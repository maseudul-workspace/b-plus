package in.net.webinfotech.udaandemo.domain.interactors;

import in.net.webinfotech.udaandemo.domain.model.User;

public interface CheckLoginInteractor {
    interface Callback {
        void onCheckLoginSuccess(User user);
        void onCheckLoginFail(String errorMsg);
    }
}
