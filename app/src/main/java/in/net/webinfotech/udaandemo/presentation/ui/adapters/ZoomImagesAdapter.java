package in.net.webinfotech.udaandemo.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.github.chrisbanes.photoview.PhotoView;

import java.util.ArrayList;

import in.net.webinfotech.udaandemo.domain.model.Testing.HomeHeaderSliders;
import in.net.webinfotech.udaandemo.util.GlideHelper;

/**
 * Created by Raj on 07-05-2019.
 */

public class ZoomImagesAdapter extends PagerAdapter{

    Context mContext;
    ArrayList<HomeHeaderSliders> headerSliders;

    public ZoomImagesAdapter(Context mContext, ArrayList<HomeHeaderSliders> headerSliders) {
        this.mContext = mContext;
        this.headerSliders = headerSliders;
    }

    @Override
    public int getCount() {
        return headerSliders.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        PhotoView imageView = new PhotoView(mContext);
        imageView.setScaleType(ImageView.ScaleType.CENTER);
        GlideHelper.setImageView(mContext, imageView, headerSliders.get(position).imageUrl);
        container.addView(imageView);
        return imageView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

}
