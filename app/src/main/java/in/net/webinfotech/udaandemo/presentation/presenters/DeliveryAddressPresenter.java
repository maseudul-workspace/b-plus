package in.net.webinfotech.udaandemo.presentation.presenters;

import in.net.webinfotech.udaandemo.presentation.ui.adapters.DeliveryAddressAdapter;

public interface DeliveryAddressPresenter {
    void fetchShippingAddress();
    void placeOrder(int addressId);
    interface View {
        void loadAdapter(DeliveryAddressAdapter adapter);
        void showLoader();
        void hideLoader();
        void hideAddressRecyclerView();
        void goToAddressEditActivity(int id);
        void onAddressSelected(int id);
        void onOrderPlaceSuccess(int orderId);
        void goToLoginActivity();
    }
}
