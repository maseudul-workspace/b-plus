package in.net.webinfotech.udaandemo.presentation.presenters;

import in.net.webinfotech.udaandemo.domain.model.Cart;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.CartAdapter;

public interface CartsPresenter {
    void fetchCartProducts();
    interface View {
        void showLoader();
        void hideLoader();
        void loadData(CartAdapter adapter, double cartTotal);
        void hideCartRecyclerView();
        void goToLoginActivity();
    }
}
