package in.net.webinfotech.udaandemo.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 28-08-2019.
 */

public class MainSliders {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("slider")
    @Expose
    public String slider;

}
