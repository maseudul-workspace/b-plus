package in.net.webinfotech.udaandemo.domain.model.Testing;

/**
 * Created by Raj on 07-05-2019.
 */

public class Thumbnails {
    public String imageUrl;
    public boolean isSelected;

    public Thumbnails(String imageUrl, boolean isSelected) {
        this.imageUrl = imageUrl;
        this.isSelected = isSelected;
    }
}
