package in.net.webinfotech.udaandemo.presentation.presenters.impl;

import android.content.Context;

import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.GetSecondCategoryInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.impl.GetSecondCategoryInteractorImpl;
import in.net.webinfotech.udaandemo.domain.model.SecondCategory;
import in.net.webinfotech.udaandemo.presentation.presenters.SecondCategoryPresenter;
import in.net.webinfotech.udaandemo.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.udaandemo.presentation.ui.adapters.SecondCategoryAdapter;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;

/**
 * Created by Raj on 29-08-2019.
 */

public class SecondCategoryPresenterImpl extends AbstractPresenter implements SecondCategoryPresenter, GetSecondCategoryInteractor.Callback, SecondCategoryAdapter.Callback {

    Context mContext;
    SecondCategoryPresenter.View mView;
    SecondCategoryAdapter adapter;
    GetSecondCategoryInteractorImpl secondCategoryInteractor;

    public SecondCategoryPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchSecondCategories(int id) {
        secondCategoryInteractor = new GetSecondCategoryInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), id);
        secondCategoryInteractor.execute();
    }

    @Override
    public void onGettingSecondCategoriesSuccess(SecondCategory[] secondCategories) {
        adapter = new SecondCategoryAdapter(mContext, secondCategories, this);
        mView.loadAdapter(adapter);
    }

    @Override
    public void onGettingSecondCategoriesFail(String errorMsg) {

    }

    @Override
    public void onCategoryClicked(int id) {
        mView.goToProductsBySellerList(id);
    }
}
