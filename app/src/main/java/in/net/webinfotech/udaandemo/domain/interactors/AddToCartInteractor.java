package in.net.webinfotech.udaandemo.domain.interactors;

public interface AddToCartInteractor {
    interface Callback {
        void onAddToCartSuccess();
        void onAddToCartFail(String errorMsg, int loginError);
    }
}
