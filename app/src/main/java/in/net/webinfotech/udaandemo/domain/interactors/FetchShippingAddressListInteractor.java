package in.net.webinfotech.udaandemo.domain.interactors;

import in.net.webinfotech.udaandemo.domain.model.Address;

public interface FetchShippingAddressListInteractor {
    interface Callback {
        void onGettingShippingAddressSuccess(Address[] addresses);
        void onGettingShippingAddressFail(String errorMsg, int isLoginError);
    }
}
