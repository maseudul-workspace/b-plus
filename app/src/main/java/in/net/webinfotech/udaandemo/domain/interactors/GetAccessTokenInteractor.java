package in.net.webinfotech.udaandemo.domain.interactors;

public interface GetAccessTokenInteractor {
    interface Callback {
        void onGettingAccessTokenSuccess(String accessToken);
        void onGettingAccessTokenFail(String errorMsg);
    }
}
