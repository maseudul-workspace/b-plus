package in.net.webinfotech.udaandemo.presentation.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.udaandemo.R;
import in.net.webinfotech.udaandemo.domain.model.Colors;

/**
 * Created by Raj on 03-09-2019.
 */

public class ColorsHorizontalAdapter extends RecyclerView.Adapter<ColorsHorizontalAdapter.ViewHolder> {

    public interface Callback {
        void onColorSelect(int id);
    }

    Context mContext;
    Colors[] colors;
    Callback mCallback;

    public ColorsHorizontalAdapter(Context mContext, Colors[] colors, Callback callback) {
        this.mContext = mContext;
        this.colors = colors;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_colors_horizontal, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.colorLayout.setBackgroundColor(Color.parseColor(colors[i].colorValue));
        if (colors[i].isSelected) {
            viewHolder.colorContainerLayout.setBackgroundColor(mContext.getResources().getColor(R.color.grey));
        } else {
            viewHolder.colorContainerLayout.setBackgroundColor(mContext.getResources().getColor(R.color.white1));
        }
        viewHolder.colorContainerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onColorSelect(colors[i].colorId);
            }
        });
    }

    @Override
    public int getItemCount() {
        return colors.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.color_container_layout)
        View colorContainerLayout;
        @BindView(R.id.color_layout)
        View colorLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateColors(Colors[] colors) {
        this.colors = colors;
        notifyDataSetChanged();
    }

}
