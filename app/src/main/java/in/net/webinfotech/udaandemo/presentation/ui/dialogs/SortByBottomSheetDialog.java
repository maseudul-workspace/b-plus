package in.net.webinfotech.udaandemo.presentation.ui.dialogs;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import in.net.webinfotech.udaandemo.R;

/**
 * Created by Raj on 29-04-2019.
 */

public class SortByBottomSheetDialog extends BottomSheetDialogFragment {

    public interface Callback{
        void onRadioBtnSelected(String msg);
    }
    Callback mCallback;
    String selectedText;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle mArgs = getArguments();
        selectedText = mArgs.getString("selectedText");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sort_by_bottom_sheet_layout, container, false);
        RadioGroup sortByRadioGroup = view.findViewById(R.id.radio_group_sort_by);
        RadioButton titleAscRadioBtn = view.findViewById(R.id.radio_btn_price_title_asc);
        RadioButton titleDescRadioBtn = view.findViewById(R.id.radio_btn_price_title_desc);
        RadioButton newestArrivalRadioBtn = view.findViewById(R.id.radio_btn_new_arrivals);
        RadioButton priceHighRadioBtn = view.findViewById(R.id.radio_btn_price_high);
        RadioButton priceLowRadioBtn = view.findViewById(R.id.radio_btn_price_low);
        switch (selectedText){
            case "Newest Arrivals":
                newestArrivalRadioBtn.setChecked(true);
                break;
            case "Title: A - Z":
                titleAscRadioBtn.setChecked(true);
                break;
            case "Title: Z - A":
                titleDescRadioBtn.setChecked(true);
                break;
            case "Price: High to Low":
                priceHighRadioBtn.setChecked(true);
                break;
            case "Price: Low to High":
                priceLowRadioBtn.setChecked(true);
                break;

        }
        sortByRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton checkedRadioBtn = (RadioButton)radioGroup.findViewById(i);
                mCallback.onRadioBtnSelected(checkedRadioBtn.getText().toString());
                dismiss();
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (Callback) context;
        } catch (ClassCastException e){
            throw new ClassCastException(context.toString() + "must implement methods");
        }
    }
}
