package in.net.webinfotech.udaandemo.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserDetails {

    @SerializedName("user")
    @Expose
    public User user;

    @SerializedName("user_profile")
    @Expose
    public UserProfile userProfile;

}
