package in.net.webinfotech.udaandemo.domain.interactors.impl;


import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.FetchStateListInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.udaandemo.domain.model.StateList;
import in.net.webinfotech.udaandemo.domain.model.StateListWrapper;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;

public class FetchStateListInteractorImpl extends AbstractInteractor implements FetchStateListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;

    public FetchStateListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingStateListFail();
            }
        });
    }

    private void postMessage(StateList[] states){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingStateListSuccess(states);
            }
        });
    }

    @Override
    public void run() {
        StateListWrapper stateListWrapper = mRepository.fetchStateList();
        if (stateListWrapper == null) {
            notifyError();
        } else if (!stateListWrapper.status) {
            notifyError();
        } else {
            postMessage(stateListWrapper.stateLists);
        }
    }
}
