package in.net.webinfotech.udaandemo.domain.interactors.impl;

import in.net.webinfotech.udaandemo.domain.executor.Executor;
import in.net.webinfotech.udaandemo.domain.executor.MainThread;
import in.net.webinfotech.udaandemo.domain.interactors.GetFirstCategoriesInteractor;
import in.net.webinfotech.udaandemo.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.udaandemo.domain.model.FirstCategory;
import in.net.webinfotech.udaandemo.domain.model.FirstCategoryWrapper;
import in.net.webinfotech.udaandemo.domain.model.MainData;
import in.net.webinfotech.udaandemo.repository.AppRepositoryImpl;
import in.net.webinfotech.udaandemo.util.GlideHelper;

/**
 * Created by Raj on 28-08-2019.
 */

public class GetFirstCategoriesInteractorImpl extends AbstractInteractor implements GetFirstCategoriesInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    int id;

    public GetFirstCategoriesInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, int id) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.id = id;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingFirstCategoriesFail(errorMsg);
            }
        });
    }

    private void postMessage(final FirstCategory[] firstCategories){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingFirstCategoriesSuccess(firstCategories);
            }
        });
    }


    @Override
    public void run() {
        FirstCategoryWrapper firstCategoryWrapper = mRepository.fetchFirstCategories(id);
        if(firstCategoryWrapper == null) {
            notifyError("Something went wrong");
        } else if(!firstCategoryWrapper.status) {
            notifyError(firstCategoryWrapper.message);
        } else {
            postMessage(firstCategoryWrapper.firstCategories);
        }
    }
}
